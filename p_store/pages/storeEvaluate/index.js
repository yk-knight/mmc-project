const wxReq = require("../../../service/request.js")

let _type = 0;
let _storeId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemNew:false,
    evaData:{},
    evaInfo:{
      storeStar: 0
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _storeId = options.storeId;
    console.log(options);
    this.setData({
      'evaInfo.storeStar': options.storeStar
    })
    this.getEvaluate()
  },
  showNewEva(){

    this.setData({
      itemNew: !this.data.itemNew
    })
    let _itemNew = this.data.itemNew;

    if (_itemNew){
      _type = 1
    }else{
      _type = 0
    }
    this.getEvaluate();
  },
  // 获取评论列表
  getEvaluate(){
    let that = this;
    wxReq.HttpRequst(false, 'api/store/GetStoreEvaluate', { storeId: _storeId, type:_type, sign:'' }, 'POST', (result) => {
      that.setData({
        evaData: result
      })
      
    })
  }
  
})