const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
let pages = utils.initPage();
const app = getApp();
let _cusId = "";
let apiData = {};
let keyword = "";
let _ViewDic = 0;
let _storeList = "";
let sortType = 1;
let _procode = "";
let _cid = '';
let _lat = 0;
let _lng = 0;
let _storeData = ""; //离线缓存
let _recData = "";  //离线缓存
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isEnd: false,
    isLoading: false,
    topLoading:false,
    searchWord:'',
    userLocation:{},
    starSearch:false,
    focus:false,
    noSearchRes:false,
    isSearch: false, //是否为搜索的结果
    recommendStoreList: [],
    storeList: [],
    searchHistory:[],
    viewDic:[
      { id: 0, name: '全部' },
      { id: 1, name: '已消费' },
      { id: 2, name: '已关注' },
    ],
    viewDicIndex:0,
    sort:[
      {id:1,name:'距离最近'},
      {id:2,name:'好评优先'}
    ],
    screen: [
      { id: '', name: '全部' },
      { id: '010101', name: '剪发' },
      { id: '010102', name: '染发' },
      { id: '010103', name: '烫发' },
      { id: '010104', name: '头发护理' },
      { id: '010199', name: '其他美发' },
      { id: '010200', name: '美容' },
      { id: '010300', name: '美甲' },
      { id: '010400', name: '美睫' },
      { id: '010500', name: 'SPA' },
      { id: '010600', name: '医美' }
    ],
    screenIndex:0,
    sortIndex:0
  },
  // 切换消费关注tab
  changeList(e){
    this.setData({
      viewDicIndex: e.currentTarget.dataset.view
    })
    
    this.GetStoreList(pages);
  },
  toStore:function(e) {

    let _store= e.currentTarget.dataset.store
    app.globalData.storeInfo = _store;
    wx.navigateTo({
      url: `/p_store/pages/B-homePage/index?StoreId=${_store.StoreId}`,
    })
  },
  bindSortChange(e){
    this.setData({
      sortIndex: e.detail.value
    })
    sortType = this.data.sort[e.detail.value].id
    this.GetStoreList(pages)
  },
  bindScreenChange(e) {
    _procode = this.data.screen[e.detail.value].id
    this.setData({
      screenIndex: e.detail.value
    })
    this.GetStoreList(pages)
  },

  //获取店铺列表
  GetStoreList: function(_page){

    let that = this;
    let _showList = [];

    apiData.customerId = _cusId;
    apiData.p = _page;
    apiData.sortby = sortType;
    apiData.keyword = this.data.searchWord
    apiData.procode = _procode
    apiData.ViewDic = this.data.viewDicIndex

    if (_page.PageIndex == 0) {
      this.setData({
        topLoading: true,
		  isEnd: false
      })
    } else {
      _showList = this.data.storeList
    }
    let _list = that.data.storeList;
    wxReq.HttpRequst(false, 'api/store/GetStoreList', 
      apiData,'POST',
      (result) => {


        this.setData({
          topLoading: false,
          isLoading: false
        })
        
		  if (_page.PageIndex > 0 && result.length<1){
			  this.setData({
				  isEnd:true
			  });
			  return;
		  }
        if(!result.length){
          if(this.data.isSearch){

            if (!that.data.recommendStoreList.length) {
              that.getRecommendList();
            } else {
              that.setData({
                storeList: _showList.concat(that.data.recommendStoreList)
              })
            }
          }else{

            that.setData({
              storeList: _showList.concat(result)
            })
          }
          that.setData({
            noSearchRes:true
          })

        }else{

          that.setData({
            storeList: _showList.concat(result),
            noSearchRes:false
          })
        }
      })
  },

  // 获取人气推荐店铺和近期消费店铺
  getRecommendList() {

    const that = this;

    wxReq.HttpRequst(false, 'api/MMC/HomeRecommend',
      { customerId: _cid, latitude: _lat, longitude: _lng }, 'POST', (res) => {

        let _strRes = JSON.stringify(res)

        if (_recData !== _strRes) {
          wx.setStorageSync("recData", _strRes);

          if (res) {
            that.setData({
              recommendStoreList: res.recommendStoreList
            });
          }

        }

      })
  },

  // 加载离线缓存数据
  GetStorageData() {
    _storeData = wx.getStorageSync("storeData"); //读取离线缓存
    _recData = wx.getStorageSync("recData"); //读取离线缓存

    // banner，新店打榜数据
    if (_storeData && _storeData != "") {

      let _listData = JSON.parse(_storeData)

      this.setData({
        bannerList: _listData.bannerList,
        // newStoreList: _listData.newStoreList,
      });
    };

    //人气推荐数据
    if (_recData && _recData != "") {

      let _listData = JSON.parse(_recData)

      this.setData({
        recommendStoreList: _listData.recommendStoreList,
      });
    }

  },

  // 监控搜索框
  watchInput(event) {
    this.setData({
      searchWord: event.detail.value
    })
  },
  beginSearch(){
    this.ShowSearchPage(true)
  },
  tagSearch(option){
    this.setData({
      starSearch: false,
      searchWord: option.currentTarget.dataset.word,
      isSearch: true
    })
    pages.PageIndex = 0;
    this.GetStoreList(pages)
  },
  getSearch(){
    this.setData({
      starSearch: false
    })

    if(this.data.searchWord != ''){
      let _searchHistory = this.data.searchHistory;
      if (_searchHistory.length > 0) {
        _searchHistory[_searchHistory.length] = this.data.searchWord;
        wx.setStorageSync('searchHistory', _searchHistory.join(','))
      } else {
        wx.setStorageSync('searchHistory', this.data.searchWord)
      }
      this.setData({
        isSearch: true
      })
    }
    pages.PageIndex = 0;
    this.GetStoreList(pages)
  },
  // 切换搜索页，展示搜索历史
  ShowSearchPage(ts){
    // 展示搜索页
    if (ts) {
      this.setData({
        focus: true,
        starSearch: true
      });
      let _searchHistory = wx.getStorageSync('searchHistory');
      if (_searchHistory) {
        this.setData({
          searchHistory: _searchHistory.split(',')
        })
      }
    }
  },
  // 获取用户当前位置
  getLatLng() {
    let that = this;
    wx.getLocation({
      success: function (res) {
        _lat = res.latitude;
        _lng = res.longitude;

        that.setData({
          userLocation: res
        })
        // 调用banner和新店列表

      }
    })
  },
  onShow: function(){
    _cid = wx.getStorageSync("sessionId")
    this.GetStorageData()

    this.getLatLng();

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    _storeList = wx.getStorageSync("storeList");
    if(_storeList){
      this.setData({
        storeList: _storeList
      })
    }
    this.ShowSearchPage(options.toSearch);
    apiData.viewcode = options.viewcode;
    _cusId = wx.getStorageSync("sessionId")

    // 提供后台当前坐标用于排序
    utils.getUserLocation()
      .then(res => {
        _lat = res.latitude;
        _lng = res.longitude;
        apiData.latitude = res.latitude;
        apiData.longitude = res.longitude;
        this.GetStoreList(pages)

      }).catch(res => {

        console.log(res)

      })
  },
  // 上拉加载分页
  loadMore() {

    if (this.data.isEnd) {
      return;
    }
    this.setData({
      isLoading: true
    })
    pages.PageIndex++
    this.GetStoreList(pages);
  }
  
})