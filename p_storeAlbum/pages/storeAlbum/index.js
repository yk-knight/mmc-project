const wxReq = require("../../../service/request.js")
let _customerId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storePhoto:[]
  },
  // 点击查看图片
  showPhoto(e) {

    let _photoList = this.data.storePhoto.map(function (item) {
      return item.FilePath;
    });

    wx.previewImage({
      current: e.currentTarget.dataset.path, // 当前显示图片的http链接
      urls: _photoList, // 需要预览的图片http链接列表
      success: function (e) {

      }
    })
  },
  // 获取门店相册
  GetStorePics(storeId) {
    let that = this;
    wxReq.HttpRequst(false, 'api/MMC/GetStoreBannerById',
      { storeId: storeId, customerId: _customerId },
      'POST',
      function (result) {
        if (result.bannerList.length > 0) {
          that.setData({
            storePhoto: result.bannerList,
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _customerId = wx.getStorageSync('sessionId')
    this.GetStorePics(options.storid)
  }
})