    // pages/perfectInformation/index.js
const Util = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
const config = require('../../../service/config.js')
const app = getApp()
let _Signs = ""
// 图片上传
let successUp = 0; //成功
let failUp = 0; //失败
let length = 0; //总数
let count = 0; //第几张
let isLoad = false;
const baseUrl = config.getBaseUrl();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sexIndex:0,
    sex: [
      {
        id: '0',
        name: '保密'
      },
      {
        id: '1',
        name: '男'
      },
      {
        id: '2',
        name: '女'
      }
    ],
    age:"1990-01-01",
    startYear:"1910-01-01",
    endYear:"",
    region: ['江苏省', '南京市','雨花台区'],
    ageIndex:[0,0,0],
    customItem: '全部',
    merry:['单身','恋爱中','已婚','离异/丧偶'],
    infoSubmit:{
      CustomerId: wx.getStorageSync("sessionId"),
      Gender:'',
      Birthday:'',
      YearOld:0,
      UserSign:'',
      constellation:'',
      Marriage:"",
      City: ['江苏省', '南京市', '雨花台区'],
      Signs: [],
      MyTitle:''
    }
  },
  // 个人资料存到全局变量
  saveInfo(){
    app.globalData.infoSubmit = this.data.infoSubmit;
  },
  // 选择个性标签
  toSelectTags(){
    wx.navigateTo({
      url: '/p_perfectInformation/pages/signTags/index',
    })
  },
  // 选择性别
  bindPickerChange: function(e){
    let _index = e.detail.value;

    this.setData({
      'infoSubmit.Gender': this.data.sex[_index].id
    })
    this.saveInfo();
  },

  // 年龄选择
  bindAgeChange: function(e){
    let _date = e.detail.value;
    let _age = app.GetAges(_date);
    let _constellation = app.GetConstellation(_date)

    this.setData({
      age:_date,
      "infoSubmit.Birthday": _date,
      "infoSubmit.YearOld": _age,
      "infoSubmit.constellation": _constellation
    })
    this.saveInfo();
  },
  // 选择婚恋情况
  bindMerryChange:function(e) {
    let _Index = e.detail.value;
    this.setData({
      'infoSubmit.Marriage': this.data.merry[_Index]
    })
    this.saveInfo();
  },
  // 选择城市
  bindCityChange: function(e){
    let _city = e.detail.value
    this.setData({
      'infoSubmit.City': _city[1]
    })
  },
  // 选择封面
  addPics() {
    let that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        
        that.setData({
          "infoSubmit.coverPic": tempFilePaths[0]
        });
        that.updateCover(tempFilePaths, successUp, failUp, count,1);
      }
    })
  },
  // 上传封面
  updateCover(imgPaths, successUp, failUp, count, length){
    console.log(imgPaths[count])
    let reqData={
      customerId: wx.getStorageSync("sessionId")
    }
      let that = this;

      wx.showLoading({
        title: '正在上传第' + count + '张',
      })
      wx.uploadFile({
        url: baseUrl + 'api/Customer/UploadCustomerPhoto',
        filePath: imgPaths[count],
        name: 'file' + count,
        header: {
          "content-type": 'application/x-www-form-urlencoded'
        },
        formData: reqData,
        success: function (res) {
          successUp++; //成功+1
        },
        fail: function (res) {
          failUp++; //失败+1
          that.uploadCallBack(photoId);
        },
        complete: function (res) {
          count++; //下一张
          if (failUp > 0) {
            that.uploadCallBack(photoId);
            return;
          }
          if (count == length) {
            //上传完毕，作一下提示
            wx.showToast({
              title: '上传成功' + successUp,
              icon: 'success',
              duration: 2000
            });
          } else {
            //递归调用，上传下一张
            that.UploadFiles(imgPaths, successUp, failUp, count, length, photoId);
          }
        },
      })
  },
  // 移除图片
  // removePic(e) {
  //   this.setData({
  //     "infoSubmit.coverPic": ""
  //   })
  // },
  setText(e){
    this.setData({
      "infoSubmit.MyTitle": e.detail.value
    })
  },
  submitUserInfo() {
    wxReq.HttpRequst(false, 'api/Customer/UpdateCustomerInfo',
      this.data.infoSubmit,
      'POST',
      function (res) {
				wx.showToast({
					title: '保存成功！',
				})
				setTimeout(function(){
					wx.navigateBack()
				},1500)
        
      })
  },
  // 读取现有资料
  getUserInfo(){
    let that = this;
    let _customerId = wx.getStorageSync("sessionId")
    wxReq.HttpRequst(false, 'api/Customer/GetCustomerInfo',
      { customerId: _customerId},
      'POST',
      function (res) {
        let _info = res.user;
        _info.constellation = ""
        // app.globalData.labelContent = _info.LabelContent;
        if (res.affixs.length > 0){
          _info.coverPic = res.affixs[0].AffixPath;
        }
        if (_info.Birthday) {
          _info.constellation = app.GetConstellation(_info.Birthday);
        }
        that.setData({
          infoSubmit: _info
        })
      })
  },
  onShow: function (options) { 
    if (app.globalData.labelContent != this.data.infoSubmit.LabelContent ){
      this.setData({
        'infoSubmit.LabelContent': app.globalData.labelContent
      })
    }   
    
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onLoad: function (options) {
      this.getUserInfo();
  }
})