const wxReq = require("../../../service/request.js");
const utils = require('../../../utils/util.js');
var app = getApp();
let pages = utils.initPage();
let _customerId = "";  //被查看主页的用户的
let _myCustomerId = ""; //当前用户的
let updateInfo = 0;
let _userLocation = {};
let _userInfo = {};
let newProduct = [];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    myCustomerId: '', //当前用户的
    userInfo:{},
    affixs: [], //封面
    userLocation: {},
    product:[]
  },

  getDesignerInfo(){
    let that = this;
    wxReq.HttpRequst(false, 'api/storeuser/GetBUserInfo_UnSAAS',
      { bucustomerId: _customerId, customerId: _myCustomerId},
      'POST', (result)=>{
        console.log(result)
        wx.setNavigationBarTitle({
          title: `${result.user.Name}的主页`
        })
        that.setData({
          userInfo: result.user,
          affixs: result.affixs,
        })
        _userInfo = result.user;
        updateInfo = 0;
      })
  },

  //作品展示
  getWorks(){
    let that = this;
    wxReq.HttpRequst(false, "api/customerphoto/GetPhotoList",
      { customerId: _myCustomerId, p: pages, viewFlag: 0, viewcustomerId: _customerId},
       'POST',(result)=>{
        console.log(result)
         that.setData({
           product: result
         })
         newProduct = result;
      })
  },

  // 关注虫虫用户
  getAttention(event) {
    let that = this;
    let _cusId = wx.getStorageSync('sessionId');
    let _attention = !this.data.userInfo.IsLike
    wxReq.HttpRequst(false, 'api/Customer/AttentionAction',
      { customerId: _cusId, dataid: _customerId, AttentionType: 3, optType: _attention ? 0 : 1 },
      'POST', function (res) {
        console.log(res)
        that.setData({
          'userInfo.IsLike': _attention
        })
      })
  },

  // 拨打电话
  callTel(e) {
    app.CallTel(e.currentTarget.dataset.tel)
  },
  // 打开地图导航
  getMap() {
    let _GPS = new Object();
    _GPS.latitude = parseFloat(_userInfo.GPSStoreLatitude);
    _GPS.longitude = parseFloat(_userInfo.GPSStoreLongitude);
    utils.GetMapLocation(_userInfo.GPSStoreName, _userInfo.GPSStoreAddrees, _GPS)
  },

  // 获取用户当前位置
  getLatLng() {
    let that = this;
    utils.getUserLocation().then(res => {
      console.log(res)
      that.setData({
        userLocation: res
      }, function () {
      })
    })
  },

  //查看帖子详情
  toPhotoDetail(e) {
    let _pid = e.currentTarget.dataset.pid;
    wx.navigateTo({
      url: `/p_show/pages/showInfo/index?photosId=${_pid}&customerId=${_customerId}`
    })
  },
  // 长按删除帖子
  longTap(e) {
    let that = this;
    let _pid = e.currentTarget.dataset.pid;

    if (_myCustomerId != _customerId) {
      return;
    }
    wx.showModal({
      title: '',
      content: '是否删除？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '正在删除...'
          });
          wxReq.HttpRequst(false, 'api/customerphoto/DeletePhoto',
            { customerId: _myCustomerId, photoId: _pid },
            'POST', function (res) {
              wx.hideLoading()
              that.getWorks()
            })
        } else if (res.cancel) {
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _myCustomerId = wx.getStorageSync('sessionId');
    this.setData({
      myCustomerId: _myCustomerId
    })
    var arr = Object.keys(options);

    if (arr.length == 0) {
      _customerId = _myCustomerId  //自己查看自己的个人主页
    } else {
      _customerId = options.customerId //查看别人的个人主页
    }

    if (updateInfo == 0) {
      updateInfo = 1;
      this.getDesignerInfo();
    }
    this.getWorks();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (updateInfo == 0) {
      updateInfo = 1;
      this.getDesignerInfo();
    }
    this.getWorks();
    this.getLatLng()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let _rName = _userInfo.Name;
    let sharePara = {
      title: _rName + "的主页",
      path: `/p_authenticate/pages/authenticatedHome/index?customerId=${_customerId}`
    }
    wxReq.HttpRequst(false, 'api/Customer/CustomerShare',
      { customerId: _myCustomerId, shareType: 6, dataId: _customerId, },
      'POST',
      function (result) {

      })
    return sharePara;
  }
})