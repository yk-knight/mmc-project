// 拼拼乐
const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
var app = getApp();
let _customerId = '';

let _hotGroup = [
  {
    leader: '小笨笨',
    leaderImg: '../../img/userImg1.png',
    requestNum: '1',
    remainTime: '23:44:37:5',
    endTime: '2019/04/29 23:00:00',
  },
  {
    leader: '素锦流年',
    leaderImg: '../../img/userImg2.png',
    requestNum: '1',
    remainTime: '23:44:37:5',
    endTime: '2019/05/01 23:00:00',
  }
]
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: false,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 0,
    nextMargin: 0,
    current: 1,
    groupBanner:[
      {
        FilePath: '../../img/banner1.jpg',
      },
      {
        FilePath: '../../img/banner1.jpg',
      },
      {
        FilePath: '../../img/banner1.jpg',
      }
    ],
    groupContent: {
      title: "沙宣单人技术总监洗剪吹",
      groupPrice: "199",
      originalPrice: "599",
      groupedNum: "20",
      coverSrc: "../../img/cover-img.jpg",
      stock: 10
    },
    hotGroup:[],
    groupProgress:[
      {
        id: 1,
        name: '选择开团'
      },
      {
        id: 2,
        name: '参团支付'
      },
      {
        id: 3,
        name: '拼团成功'
      }
    ],
    popHidden: true,
    buyNum: 1
  },

  getChange(e) {
    this.setData({
      current: e.detail.current + 1
    })
  },


  onShareAppMessage(res) {
    var pageTitle = this.data.groupContent.title;
    return {
      title: pageTitle,
      path: `p_groupBuying/pages/groupBuy/index`
    }
  },

  // 开团
  startGroup(){
    if (this.data.popHidden){
      this.setData({
        popHidden: false
      })
    }else{
      // 支付
      wxReq.HttpRequst(false,
        'api/WxOpen/WXJsApiPay',
        { Fee: 1, customerId: _customerId, OrderBusiness: 2, Description: "测试支付" },
        'POST',
        (res) => {
          console.log(res);
          let _data = res;
          wx.requestPayment({
            timeStamp: _data.timeStamp,
            nonceStr: _data.nonceStr,
            package: _data.package,
            signType: _data.signType,
            paySign: _data.paySign,
            success: function (res) {
              wx.showModal({
                title: '支付成功',
                content: '',
                success(res) {
                  wx.redirectTo({
                    url: '../groupBuyDetail/index',
                  })
                }
              })
            },
            fail: function (res) {
              console.log(res);
            }
          })
        })
    }
  },

  // 关闭弹框
  closePop(){
    this.setData({
      popHidden: true
    })
  },

  // 数量操作
  reduce(){
    var _num = this.data.buyNum;
    if(_num>1){
      _num--;
    }else{
      return false;
    }
    this.setData({
      buyNum: _num
    })
  },
  add() {
    var _num = this.data.buyNum;
    console.log(this.data.groupContent.stock)
    if (_num < this.data.groupContent.stock){
      _num++
    }
    this.setData({
      buyNum: _num
    })
  },
  inputChange(e){
    var _num = parseInt(e.detail.value);
    if (_num >= 1 && _num <= this.data.groupContent.stock){
      this.setData({
        buyNum: _num
      })
    }
  },
  //小于10的格式化函数
  timeFormat(param) {
    return param < 10 ? '0' + param : param;
  },
  //倒计时函数
  countDown() {
    // 获取当前时间
    let newTime = new Date().getTime();

    // 对结束时间进行处理渲染到页面
    _hotGroup.forEach(o => {
      let endTime = new Date(o.endTime).getTime();
      let _remainTime = '';

      if (endTime - newTime > 0) {
        //将毫秒数的time转为分秒
        let time = (endTime - newTime) / 100;  
        // 获取时、分、秒、分秒
        _remainTime = this.dealerTime(time)

      } else {
        //活动已结束，全部设置为'00'
        _remainTime = "00:00:00:00"

      }
      o.remainTime = _remainTime;
    })
    // 渲染，然后每隔一秒执行一次倒计时函数
    this.setData({ hotGroup: _hotGroup })
    setTimeout(this.countDown, 100);
  },
  // 处理时间
  dealerTime(time){
    // 保留到分秒，所以此处time为分秒数
    let remainTimeArr = new Array();
    let remainTime = '';
    let hou = parseInt(time / 36000);
    let min = parseInt(time % 36000 / 600);
    let sec = parseInt(time % 36000 % 600 / 10);
    let mSec = parseInt(time % 36000 % 600 % 10);
    remainTimeArr = [this.timeFormat(hou), this.timeFormat(min), this.timeFormat(sec), mSec];
    remainTime = remainTimeArr.join(":");

    return remainTime;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _customerId = wx.getStorageSync('sessionId');

    // 执行倒计时函数
    this.countDown();
  }

})