const wxReq = require("../../../service/request.js")
let _cid = ""
let _sortOrder = [];
let sortArray = []
let sortArrayDef = []
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sortList:[],   
    chooseId:0,
    switchCheck:false
  },
  // 是否使用默认顺序
  ChangeTechnician(e) {
    this.setData({
      switchCheck: e.detail.value
    })
    
  },
  // 获取支付顺序
  getPaySort(){
    let that = this;

    wxReq.HttpRequst(false,
      'api/Customer/ReadPayOrder',
      { customerId: _cid },
      'POST',
      function (res) {
        let isDef = false
        sortArrayDef = res.DefaultOrderstr;
        if (res.CustomerOrder){
          _sortOrder= res.CustomerOrder
          sortArray = res.CustomerOrderstr.split(",")
        }else{
          _sortOrder= res.DefaultOrder
          sortArray = res.DefaultOrderstr.split(",")
        }
        if (res.CustomerOrderstr == res.DefaultOrderstr){
          isDef = true
        }
        that.setData({
          sortList: _sortOrder,
          switchCheck: isDef
        })
      })
  },
  // 选择某支付顺序
  chooseSort(event){
    let _id = event.currentTarget.dataset.id;
    this.setData({
      chooseId:_id
    })
  },
  // 向上移动
  moveUp(e){
    let index = e.currentTarget.dataset.index;
    if(index < 1){
      return;
    }else{
      let delSort = _sortOrder.splice(index, 1);
      let delStr = sortArray.splice(index, 1);
      _sortOrder.splice(index - 1, 0, delSort[0]);
      sortArray.splice(index - 1, 0, delStr[0]);

      this.setData({
        sortList: _sortOrder
      })
    }
  },
  // 向上移动
  moveDown(e) {
    let index = e.currentTarget.dataset.index;
    if (index == _sortOrder.length - 1) {
      return;
    } else {
      let delSort = _sortOrder.splice(index, 1);
      let delStr = sortArray.splice(index, 1);
      _sortOrder.splice(index + 1, 0, delSort[0]);
      sortArray.splice(index + 1, 0, delStr[0]);

      this.setData({
        sortList: _sortOrder
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _cid = wx.getStorageSync("sessionId");
    this.getPaySort();
  },
  onUnload:function(){
    let payOrder = ""
    let isSwitchCheck = this.data.switchCheck

    if (isSwitchCheck){
      // 使用系统默认顺序
      payOrder = sortArrayDef
    }else{
      payOrder = sortArray.join(",")
    }
    wxReq.HttpRequst(false,
      'api/Customer/ChangePayOrder',
      { customerId: _cid, PayOrder: payOrder},
      'POST',
      function (res) {
        console.log(res);
      })
  }
})