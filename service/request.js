const config = require('./config.js')

module.exports = {
  HttpRequst: HttpRequst
}

var _code = "";

// let btnSubmit = true;
//sessionChoose 1是带sessionID的GET方法  2是不带sessionID的GET方法, 3是带sessionID的Post方法,4是不带sessionID的Post方法
//ask是是否要进行询问授权，true为要，false为不要
//sessionChoose为1,2,3,4,所以paramSession下标为0的则为空
function HttpRequst(loading, url, params, method, callBack) {
  if (loading) {
    wx.showToast({
      title: '数据加载中',
      icon: 'loading'
    })
  }
  let _token = "";
  var paramSession = [
    {},{ 'content-type': 'application/json' },
    { 'content-type': 'application/x-www-form-urlencoded' }
  ];
  _code = wx.getStorageSync("code");

  // 公用ajax请求
  wx.request({
    url: config.getBaseUrl() + url,
    data: params,
    dataType: "json",
    header: { 'content-type': 'application/json' },
    method: method,
    success: function (res) {
      console.log(res)
      if (loading == true) {
        wx.hideToast();//隐藏提示框
      }
      if (res.statusCode == 200) {
        callBack(res.data);
      } else {

        wx.showToast({
          title: res.data.ExceptionMessage,
          icon: 'none',
          duration: 2000
        })
      }
    },
    fail:function(res){
      callBack(res)
      wx.showToast({
        title: res.data,
        icon: 'none',
        duration: 2000
      })
    },
    complete: function () {

      if (loading == true) {
        wx.hideToast();//隐藏提示框
      }
    }
  })
}