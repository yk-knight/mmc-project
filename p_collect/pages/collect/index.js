const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
const app = getApp()
let pages = utils.initPage();
var _cosId = "";
var _vf = -2;
let _showList = [];
let startX = 0; //开始坐标
let startY = 0;
Page({
  data: {
    tabs: [
      { name: '全部', value: 0 },
      // { name: '推荐', value: 1 },
      { name: '热门', value: 2 },
      { name: '造型秀', value: 4 },
      { name: '关注', value: -1 }
    ],
    isLoading: false,
    topLoading: false,
    noData: false,
    newPhoto: 0,
    tabIndex: 0,
    group: '',
    height: '',
    isMyLike: false,
    isEnd: false,
    showList: [],
    isPopHide: false
  },
  // 快速去对应的门店
  toStore(e) {
    let _oInfo = e.currentTarget.dataset.info;
    wx.navigateTo({
      url: `/p_store/pages/B-homePage/index?StoreId=${_oInfo.StoreId}&StoreName=${_oInfo.StoreName}`,
    })
  },
  // 更新关注秀友内容时间
  getPhotoTime() {
    let that = this;
    wxReq.HttpRequst(false, "api/Customer/UpdateNewPhotoTime",
      { customerId: _cosId }, 'POST',
      function (res) {
        that.setData({
          newPhoto: 0,
        })
      })
  },

  // 去用户主页
  toUserHome(event) {
    let cid = event.currentTarget.dataset.cid;
    wx.navigateTo({
      url: `/p_userHome/pages/userHome/index?customerId=${cid}`,
    })
  },
  // 去帖子详情页
  toShowInfo(event) {
    let _data = event.currentTarget.dataset;
    wx.navigateTo({
      url: `/p_show/pages/showInfo/index?photosId=${_data.photosid}&customerId=${_data.cid}`,
    })
  },

  // 点赞
  getLike(e) {

    let that = this;
    let _index = e.currentTarget.dataset.index;
    let _pid = e.currentTarget.dataset.pid;
    let _isLike = 'showList[' + _index + '].IsLike';
    let _likeCount = 'showList[' + _index + '].LikesCount';
    let _likeCountNum = this.data.showList[_index].LikesCount;
    let _isAddLike = this.data.showList[_index].IsLike;
    if (_isAddLike) {
      _likeCountNum--
    } else {
      _likeCountNum++
    }
    wxReq.HttpRequst(false, "api/customerphoto/AddPhotoLike",
      { photoId: _pid, customerId: _cosId, isAddLikes: !_isAddLike }, 'POST',
      function (res) {
        that.setData({
          [_isLike]: !_isAddLike,
          [_likeCount]: _likeCountNum
        })
      })
  },
  // 点击查看图片
  showPhoto(e) {

    let _cid = e.currentTarget.dataset.cid,
      _pid = e.currentTarget.dataset.pid;

    wx.navigateTo({
      url: `/p_show/pages/showInfo/index?photosId=${_pid}&customerId=${_cid}`,
    })
  },
  //   获取斗秀场列表
  GetShowList() {

    let that = this;

    let _pIndex = pages.PageIndex;

    let _showList = [];

    if (_pIndex == 0) {
      this.setData({
        topLoading: true
      })

    } else {
      _showList = this.data.showList
    }

    wxReq.HttpRequst(false, "api/customerphoto/GetPhotoList",
      { customerId: _cosId, p: pages, viewFlag: _vf }, 'POST',
      function (res) {

        let _length = res.length;

        that.setData({
          showList: _showList.concat(res),
          topLoading: false,
          isLoading: false
        })

        // 判断是否为第一页
        if (_pIndex == 0) {
          if (_length < 1) {
            that.setData({
              noData: true
            })
          }
        } else {
          if (_length < 10) {
            that.setData({
              isEnd: true
            })
          }
        }

      })
  },
  // 判断是否有最新关注的消息
  getNewPhoto() {
    let that = this;
    wxReq.HttpRequst(false, "api/Customer/MyMsgTip",
      { customerId: _cosId }, 'POST',
      function (res) {
        that.setData({
          newPhoto: res.newphoto
        })
      })
  },
  onShow() {
    let _backGetInfo = wx.getStorageSync("isBackGetInfo")

    if (_backGetInfo) {
      _cosId = wx.getStorageSync("sessionId")
      this.GetShowList();
      this.getNewPhoto()
    }
  },
  onLoad() {
    wx.showShareMenu({
      withShareTicket: true
    })
    _cosId = wx.getStorageSync("sessionId")
    if (_cosId != "") {
      pages.PageIndex = 0;
      this.GetShowList();
    }

    this.getNewPhoto()

    let _isPopHide = wx.getStorageSync("isPopHide") || false;
    this.setData({
      isPopHide: _isPopHide
    })
  },
  // 上拉加载分页
  onReachBottom() {
    if (this.data.isEnd) {
      return;
    }
    this.setData({
      isLoading: true
    })
    pages.PageIndex++
    this.GetShowList();
  }
})