const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
let _customerId = "";
let _name = '';
let _phone = '';
let _code = '';
let _workyears = '';
let _bindedStore = "";
let _choosedStoreInfo = {};
let countFlag = false;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    workyears:[
      {
        value:'0-1',
        name: "1年以下"
      },
      {
        value:'1-3',
        name: "1年-3年"
      },
      {
        value:'3-5',
        name: "3年-5年"
      },
      {
        value:'5-10',
        name: "5年-10年"
      },
      {
        value:'10-',
        name: "10年以上"
      },
    ],
    advantages:[
      {
        color: 'red',
        icon: '../../imgs/ad-icon1.png',
        title: '作品展示',
        detail1: '上传得意作品',
        detail2: '秀给更多用户'
      },
      {
        color: 'yellow',
        icon: '../../imgs/ad-icon2.png',
        title: '赚取酬金',
        detail1: '作品预约平台手艺人',
        detail2: '获取丰厚酬金'
      },
      {
        color: 'green',
        icon: '../../imgs/ad-icon3.png',
        title: '精准拓客',
        detail1: '细分人群',
        detail2: '助手艺人快速拓客'
      },
      {
        color: 'orange',
        icon: '../../imgs/ad-icon4.png',
        title: '同行交流',
        detail1: '畅通的沟通交流环境',
        detail2: '互相学习成长'
      },
    ],
    getcodeValue: "获取验证码",
    authenticated: false,
    binded: false,
    // storeList: [
    //   {
    //     name: "Ki.Mi"
    //   },
    //   {
    //     name: "永琪"
    //   }
    // ],
    yearsIndex: -1,
    // storeIndex: -1,
    storeName: ''
  },

  bindYearsChange: function (e) {
    this.setData({
      yearsIndex: e.detail.value
    })
    _workyears = this.data.workyears[e.detail.value].value;
  },

  // 选择门店
  bindStoreChange: function (e) {
    this.setData({
      storeIndex: e.detail.value
    })
    _bindedStore = this.data.storeList[e.detail.value].StoreName;
  },

  nameInput(e){
    _name = e.detail.value
  },
  phoneInput(e){
    _phone = e.detail.value
  },
  codeInput(e){
    _code = e.detail.value
  },

  // 获取验证码
  getCode(){
    let that = this;
    if (!(/^1[34578]\d{9}$/.test(_phone))){
      wx.showModal({
        title: '提示',
        content: '手机号格式有误',
        success(res) {
          if (res.confirm) {
            // console.log('用户点击确定')
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
    }else{
      if (!countFlag) {
        wxReq.HttpRequst(false, 'api/storeuser/GetFlagCode',
          { customerId: _customerId, tel: _phone },
          'POST',
          function (result) {
            console.log(result)
            if(result.Status == "02"){
              let _url = result.BUId == 0 ? `/p_authenticate/pages/authenticatedHome/index?customerId=${_customerId}` : `/p_designHome/pages/designerHome/index?customerId=${_customerId}&uid=${result.BUId}&storeId=${result.StoreId}`
              wx.showModal({
                title: '',
                content: '您已经是认证手艺人啦～',
                success(res) {
                  if (res.confirm) {
                    // console.log('用户点击确定')
                    wx.navigateTo({
                      url: _url,
                    })
                  } else if (res.cancel) {
                    // console.log('用户点击取消')
                  }
                }
              })
            }else{
              let times = 60;
              countFlag = true;
              let clock = setInterval(() => {
                times--;
                if (times > 0) {
                  that.setData({
                    getcodeValue: times + 's'
                  })
                } else {
                  countFlag = false;
                  clearInterval(clock);
                  that.setData({
                    getcodeValue: '获取验证码'
                  })
                }
              }, 1000)
            }
          })
      }
    }
  },

  // 提交认证信息
  commitInfo(e){
    let that = this;
    let fromVal = e.detail.value;
    console.log(fromVal);
    utils.saveFormId(e.detail.formId)
    if (fromVal.name == ''){
      wx.showModal({
        title: '提示',
        content: '请填写姓名',
        success(res) {
          if (res.confirm) {
            // console.log('用户点击确定')
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
    } else if (!(/^1[34578]\d{9}$/.test(fromVal.tel))) {
      wx.showModal({
        title: '提示',
        content: '手机号格式有误',
        success(res) {
          if (res.confirm) {
            // console.log('用户点击确定')
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
    } else if (fromVal.code == ''){
      wx.showModal({
        title: '提示',
        content: '请填写验证码',
        success(res) {
          if (res.confirm) {
            // console.log('用户点击确定')
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
    } else if (fromVal.jobyear == -1){
      wx.showModal({
        title: '提示',
        content: '请选择从业年限',
        success(res) {
          if (res.confirm) {
            // console.log('用户点击确定')
          } else if (res.cancel) {
            // console.log('用户点击取消')
          }
        }
      })
    }else{
      fromVal.customerId = _customerId;
      wxReq.HttpRequst(false, 'api/storeuser/SaveStoreUser',fromVal,
        'POST',
        function (result) {
          wx.showToast({
            title: '认证成功',
            icon: 'success',
            duration: 1500,
            success: function(){
              setTimeout(function () {
                that.setData({
                  authenticated: true
                })
              }, 2000)
            }
          })

        })
    }
  },

  // 返回主页
  goback(){
    wx.navigateBack({
      delta: 1
    })
  },

  // 去绑定
  toBind(){
    if (this.data.binded){
      return
    }
    wx.navigateTo({
      url: '/p_authenticate/pages/chooseStore/index?tel=' + _phone
    })
  },

  // getStoreList(){
  //   let that = this;
  //   wxReq.HttpRequst(false, 'api/storeuser/ReadStoreList',
  //     { customerId: _customerId, citycode: 3201 },
  //     'POST',
  //     function (result) {
  //       that.setData({
  //         storeList: result
  //       })
  //     })
  // },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    if(options.authenticated){
      that.setData({
        authenticated: options.authenticated
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    _customerId = wx.getStorageSync('sessionId');
    // let infoString = wx.getStorageSync("choosedStoreInfo");
    // if (infoString){
    // _choosedStoreInfo = JSON.parse(infoString);
    //   this.setData({
    //     storeName: _choosedStoreInfo.name
    //   })
    // }
    // this.getStoreList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    //  _customerId = "";
    //  _name = '';
    //  _phone = '';
    //  _code = '';
    //  _workyears = '';
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    _customerId = "";
    _name = '';
    _phone = '';
    _code = '';
    _workyears = '';
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let that = this;
    let sharePara = {
      title: '认证手艺人',
    }
    wxReq.HttpRequst(false, 'api/Customer/CustomerShare',
      { customerId: _customerId, shareType: 5, dataId: 1, },
      'POST',
      function (result) {

      })
    return sharePara;
  }
})