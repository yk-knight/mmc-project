// pages/myFollow/index.js
const wxReq = require("../../../service/request.js")
let _customerId = ""
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: ['发型师', '店铺', '秀友'],
    newPhoto:0,
    tabIndex: 0,
    attentionUser:[],
    usersquery:[],
    // 技师
    user: [],
    // 关注店铺
    stores: [],

  },
  getTabs: function (e) {
    var index = e.currentTarget.id;
    let that = this;
    if (index == 2 && this.data.newPhoto > 0){
      wxReq.HttpRequst(false, 'api/Customer/UpdateNewPhotoTime',
        { customerId: _customerId },
        'POST',
        function (res){
          that.setData({
            newPhoto:0
          })
        })
    }
    this.setData({
      tabIndex: index
    })
  },
  // 获取关注列表
  getFollow(){
    let that = this
    _customerId = wx.getStorageSync("sessionId");
    wxReq.HttpRequst(false, 'api/Customer/MyAttention',
      { customerId: _customerId },
      'POST',
      function (res){
        console.log(res);
        that.setData({
          stores:res.stores,
          usersquery: res.usersquery,
          attentionUser: res.attentionUser
        })
      })
  },
  // 去门店主页
  toStore(e){
    console.log(e)
    let data = e.currentTarget.dataset;
    wx.navigateTo({
      url: `/p_store/pages/B-homePage/index?StoreId=${data.id}&StoreName=${data.name}`,
    })
  },
  // 发型师取消关注
  outQueryFollow(e){
    let _uid = e.currentTarget.dataset.id;
    let _index = e.currentTarget.dataset.index;
    let _usersquery = this.data.usersquery;
    let that = this;

    app.SetAttention(_uid, 2, 1)
      .then((res) => {
        _usersquery.splice(_index, 1)
        that.setData({
          usersquery: _usersquery
        })
      });
  },
  // 取消关注门店
  cancleFollow(e){
    let storeId = e.currentTarget.dataset.id;
    let _index = e.currentTarget.dataset.index;
    let _stores = this.data.stores;
    let that = this;

    app.SetAttention(storeId, 1, 1)
      .then((res) => {
        _stores.splice(_index, 1)
        that.setData({
          stores: _stores
        })
      })
    
  },
  // 取消关注秀友
  outFollow(e){
    let _attentionUser = this.data.attentionUser;
    let that = this;
    let Edata = e.currentTarget.dataset
    let id = Edata.id;
    let index = Edata.index
    wxReq.HttpRequst(false, 'api/Customer/AttentionAction',
      { customerId: _customerId, dataid: id, AttentionType: 3, optType:1},
      'POST', function (res) {
        _attentionUser.splice(index, 1)
        that.setData({
          attentionUser: _attentionUser
        })
      })
  },
  onShow(){
    this.getFollow()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      newPhoto:parseInt(options.newphoto)
    })
    
  },
})