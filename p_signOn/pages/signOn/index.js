const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
let _cid = "";
var _signInfo = {};
let _strDay = "";
Page({
  data: {
    signDate:[
      {
        date: 1, gift: false
      }, {
        date: 2, gift:false
      }, {
        date: 3, gift: false
      }, {
        date: 4, gift: false
      }, {
        date: 5, gift: false
      }, {
        date: 6, gift: false
      }, {
        date: 7, gift: true
      }
    ],
    signInfo:{},
    tasks:[
      {
        name: '完善个人资料',
        cost: 5,
        stauts:false,
        url:"/p_perfectInformation/pages/perfectInformation/index"
      }, {
        name: '签到',
        cost: 2,
        stauts: false,
        url: ""
      },  {
        name: '发表帖子',
        cost: 5,
        stauts: false,
        url: "/p_showCreat/pages/showCreate/index"
      }, {
        name: '评论',
        cost: 2,
        stauts: false,
        url: "/pages/showPlace/index",
        openType:"switchTab"
      }
    ],
    signNum:0,
  },
  // 获取当前用户签到和积分信息
  getUserSignInfo(){
    let that = this;
    let isSign = "tasks[1].stauts";
    wxReq.HttpRequst(false, 'api/point/GetSignInfo',
      { customerId: _cid, signDay: _strDay },
      'POST',
      (res) => {
        if (_signInfo === this.data.signInfo && Object.keys(_signInfo).length > 0 ){
          return;
        }else{
          that.setData({
            signInfo: res,
            [isSign]:res.SignStatus
          })
        }

      }) 
  },
  // 签到领取积分
  getSign(e){
    let that = this;
    utils.saveFormId(e.detail.formId);
    utils.GetUserInfo()
      .then(res => {
        wxReq.HttpRequst(false, 'api/point/SignInByDay',
          { customerId: _cid, signDay: _strDay, isPre: false },
          "POST",
          (res) => {

            wx.showToast({
              title: `获得${res}颗虫豆`,
              image: '../images/ico-sign.png'
            })
            that.getUserSignInfo();
          })
      })
    
  },
  onLoad(){
    _cid = wx.getStorageSync("sessionId");
    let _today = new Date().toLocaleDateString();
    _strDay = _today.split("/").join("-");
    this.getUserSignInfo();
  },
  onReady:function() {
    wx.loadFontFace({
      family: 'DIN',
      source: 'url("../fonts/DIN-Bold.eot")',
      success: function() {
        console.log("成功");
      }
    })
  }
})