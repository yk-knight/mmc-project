//app.js
const wxReq = require("/service/request.js")
const Util = require('/utils/util.js')
const qqMap = Util.qqmapsdk;
let userData = {
  type: 'userinfo',
  encryptedData: "",
  iv: "",
  sessionId: ""
}
let isAttention = true;
App({
  globalData: {
    isOnlogin: false,
    labelContent:'',  //个性标签
    code:'',
    sessionId:'',
    appid: '',  //appid需自己提供
    secret: '', //secret需自己提供
    openid: '', //自定义
    encryptedData:"",
    iv:"",
    storeInfo:{},
    userInfo: {},
    city: '南京',
    isCityChange:false,
    login:{},
    orderData:{},
    infoSubmit:{},
    winWidth:0,
    winHeight:0,
  },
  onLaunch: function () {

    this.UserLogin();
  },
  
  onShow:function(){
    this.upDateApp()
  },
  upDateApp: function () {//版本更新

    if (wx.canIUse('getUpdateManager')) {//判断当前微信版本是否支持版本更新

      const updateManager = wx.getUpdateManager();

      updateManager.onCheckForUpdate(function (res) {

        if (res.hasUpdate) { // 请求完新版本信息的回调

          updateManager.onUpdateReady(function () {

            wx.showModal({

              title: '更新提示',

              content: '新版本已经准备好，是否重启应用？',

              success: function (res) {

                if (res.confirm) {// 新的版本已经下载好，调用 applyUpdate 应用新版本并重启

                  updateManager.applyUpdate()

                }

              }

            })

          });

          updateManager.onUpdateFailed(function () {

            wx.showModal({// 新的版本下载失败

              title: '已经有新版本了哟~',

              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',

            })

          })

        }

      })

    } else {

      wx.showModal({// 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示

        title: '提示',

        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'

      })

    }

  },
  // 新增关注
  SetAttention:function(dataId,type,optType){
    let data = {
      customerId: wx.getStorageSync("sessionId"),
      dataId: dataId,
      attentionType: type,
      optType: optType
    }
    return new Promise((resolve, reject) => {
      wxReq.HttpRequst(false, 'api/Customer/AttentionAction', data, 'POST', (result) => {
        resolve(result)
      })
    })
    
  },
  UserLogin:function(){
    let that = this;

    return new Promise((resolve, reject) => {
      // 1、调用小程序登录接口，获取code
      Util.wxLogin()
        .then(res => {
          that.globalData.code = res.code;
          wx.setStorageSync('code', res.code);

          let _cosId = wx.getStorageSync("sessionId")

          if (_cosId == "" || !_cosId) {
            // 调用login接口，通过code换取sessionId
            Util.apiLogin(res.code)
              .then(res => {

                let result = res.data;

                // 返回数据成功执行
                if (result.success) {

                  wx.setStorageSync('sessionId', result.sessionId);

                }
                resolve(result.success);
              })
              .catch(e => {

              })

          }else{
            resolve();
          }
            

        })
        .catch(e => {
          console.log(e);
        })

    })
    
    },
  // 门店坐标收集
  CalculateDistance: function (storeItem,index,callback) {
    // 计算距离
    qqMap.calculateDistance({
      mode: 'driving',
      to: [storeItem.GPS],
      success: function (res) {
        var dr = res.result.elements;
        storeItem.distance = dr[0].distance;
        callback(index,storeItem);
      },
      fail: function (res) {
      }
    });

    // 规划路线
    
  },
  // 根据生日计算年龄
  GetAges(birthDay){
    let _dateArray = birthDay.split("-");
    let _year = new Date().getFullYear();
    return _year - parseInt(_dateArray[0])
  },
  // 根据生日计算星座
  GetConstellation(birth) {
    if(birth){
      let _dateArray = birth.split("-");
      let _constellation = Util.getAstro(parseInt(_dateArray[1]), parseInt(_dateArray[2]))
      return _constellation + "座"
    }else{
      return false;
    }
    
  },
  // 拨打电话
  CallTel(number) {
    wx.makePhoneCall({
      phoneNumber: number
    });
  },

  user: {
    name: "",
    dress: "",
    headImg: "",
    post: 0,
    remark: 0,
    order: false
  }
})