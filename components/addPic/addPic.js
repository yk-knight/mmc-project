// components/addPic/addPic.js
// 记录每个座位的位置，方便悬浮块定位，以及每次动画，各块的位移，存储单位为px，[left,top]
let seatsArray = []
// 记录当前每个图当前位于哪个座位。比如最开始添加了3张图，那么positionArray里就应该是[0,1,2]。如果位置2的移动到0的位置，那么数组就变成了[2,0,1]
let positionArray = []
// observer列表，方便删除的时候销毁
let observerArray = []
// 动画实例列表
let animationArray = []
// px和rpx的转换率 1px * rate = 1rpx
let rate = 1
// 移动块的动画实例
let animationMove = wx.createAnimation({})
// 新增按钮的动画实例
let animationAdd = wx.createAnimation({})
// 删除区域的动画实例
let animationDel = wx.createAnimation({})
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    // picList: [],
    // 图片层部分的动画列表，新增按钮除外
    animationArray: [],

    // 移动块的动画
    animationMove: {},

    // 新增按钮的动画
    animationAdd: {},

    // 删除区域的动画
    animationDel: {},

    // 图片的列表，在获取结果前，顺序固定，不会跟着界面上的可视位置改变
    imageArray: [],

    // 移动块上的图片
    selectedImg: "",

    // 座位个数，因为要计算新增按钮位置，所以座位个数是图片数量+1
    // TODO 这里有个问题没处理，就是选了9张图后，就有了10个位置。。界面上会多出一行的位置。
    seatNumber: 0,

    height: 280,
  },

  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () { 
      // 当前选中块在 imageArray中的位置
      this.selected = -1
      let ret = wx.getSystemInfoSync()
      // 判断是否是安卓机，是安卓机则要在touchmove的时候控制setData频率
      this.isAndroid = ret.system.toLocaleLowerCase().indexOf("android") != -1
      rate = 750 / ret.windowWidth // 750rpx除以当前屏幕宽度的px值，得到转换比

      // 是否显示删除区域
      this.showDel = false
      // 此次touchmove触发时间，用户控制android机上setData频率
      this.time = 0

    },
  },
  ready: function () {
    // 监听移动块和删除区域的相交
    let _observer = wx.createIntersectionObserver()
    _observer
      .relativeTo('#movingCover')
      .observe('#del', (res) => {
        if (res.intersectionRatio == 0) {
          this.delete = false
        } else {
          this.delete = true
        }
      })
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 清理聚焦定时器
     */
    clearTimer() {
      if (this.focusTimer) {
        clearTimeout(this.focusTimer)
        this.focusTimer = 0
      }
      this.step = 0
    },

    /**
     * 定义下this.step的各个值
     * 1 开始点击，将移动块移动到当前选中的位置。
     * 2 移动块移动到当前选中的位置完成，定时器开始。
     * 3 将移动块放回当前应该在的位置，完成后恢复透明度。
     * 4 该次移动完成，复位动画执行
     */
    transitionend() {
      if (this.step == 1) {
        this.focusTimer = setTimeout(() => {
          // 把移动块进行放大，透明
          animationMove.scale(1.1).opacity(0.8).step({
            duration: 200
          })

          this.setData({
            selectedImg: this.data.imageArray[this.selected],
            animationMove: animationMove.export(),
            animationArray: this.data.animationArray
          }, () => {
            // 同时隐藏掉本来这个位置的图
            animationArray[this.selected].opacity(0).step({
              duration: 10,
              timingFunction: 'step-end'
            })
            this.data.animationArray[this.selected] = animationArray[this.selected].export()
            this.setData({
              animationArray: this.data.animationArray
            }, () => {
              // 确定 selectedImg 设好了，才能开始移动移动块
              this.startMove = true
            })
          })
        }, 200)
        this.step = 2
      } else if (this.step == 3) {
        let seat = seatsArray[positionArray[this.selected]]
        // 在移动块回来后，显示在touchstart时候隐藏掉的，当前选中的这个图
        animationArray[this.selected].opacity(1).translate(seat[0], seat[1]).step({
          duration: 10,
          timingFunction: 'step-end'
        })
        this.data.animationArray[this.selected] = animationArray[this.selected].export()
        this.setData({
          animationArray: this.data.animationArray
        }, () => {
          // 确保touchstart时候隐藏掉的，当前选中的这个图已经显示，延迟隐藏移动块
          setTimeout(() => {
            this.selected = -1
            this.setData({
              selectedImg: "",
            })
          }, 200)
        })
        this.step = 4
      }
    },

    /**
     * 触摸开始，要记录开始点的位置，记录当前按中的图的序号
     */
    start(e) {
      this.time = e.timeStamp
      // disableTap为是否禁止开始，startEvent为是否开始事件，touchend和touchmove都在startEvent为true时才继续走
      // 这里这个 if else是为了控制点击的频率，在touchend里用定时器复位这俩属性。
      if (!this.disableTap) {
        this.disableTap = true
        this.startEvent = true
      } else {
        this.startEvent = false
        return
      }
      this.delete = false

      this.selected = e.currentTarget.dataset.index
      this.startX = e.changedTouches[0].pageX
      this.startY = e.changedTouches[0].pageY

      // 先把移动块位置移动到当前选中的图的位置
      // 当前选中图的座位坐标
      let seat = seatsArray[positionArray[this.selected]]

      this.selectedPos = seat
      this.step = 1
      animationMove.translate(seat[0], seat[1]).opacity(0.8).step({
        duration: 10,
        timingFunction: 'step-end'
      })
      this.setData({
        animationMove: animationMove.export(),
      })
    },

    /**
     * 触摸移动
     */
    move(e) {

      if (!this.startEvent) {
        return
      }
      this.clearTimer()
      // android上要控制频率，差距在30ms以内就直接返回
      if (this.isAndroid) {
        let time = e.timeStamp
        if (time - this.time < 20) {
          return
        } else {
          this.time = time
        }
      }
      // 表明已经move了，就不再end的时候不再进行预览
      this.trigger = true

      if (!this.startMove) {
        return
      }
      // 移动的时候，如果没显示底部删除区域，则显示底部删除区域
      if (!this.showDel) {
        animationDel.translateY(-50).step()
        this.setData({
          animationDel: animationDel.export()
        })
        this.showDel = true
      }

      let startX = e.changedTouches[0].pageX
      let startY = e.changedTouches[0].pageY
      let positionX = this.selectedPos[0] + startX - this.startX
      let positionY = this.selectedPos[1] + startY - this.startY

      // 设置移动块当前位置
      animationMove.translate(positionX, positionY).step({
        timingFunction: 'step-start'
      })
      this.setData({
        animationMove: animationMove.export(),
      })
    },

    /**
     * 触摸结束，如果是短按，且没移动过，就预览图片。
     */
    end(e) {
      if (!this.startEvent) {
        return
      } else {
        setTimeout(() => {
          this.disableTap = false
          this.startEvent = false
        }, 500)
      }
      this.clearTimer()

      if (!this.startMove) {
        if (!this.trigger) {
          // wx.previewImage({
          //   current: e.currentTarget.dataset.path, // 当前显示图片的http链接
          //   urls: this.data.imageArray
          // })
          let seat = seatsArray[positionArray[this.selected]]
          animationMove.translate(seat[0] + 1, seat[1]).opacity(0).step({
            duration: 10,
            timingFunction: 'step-end'
          })
          this.setData({
            animationMove: animationMove.export(),
          })

        }
        return
      }

      this.startMove = false
      this.trigger = false

      if (this.showDel) {
        animationDel.translateY(0).step()
        this.setData({
          animationDel: animationDel.export()
        })
        this.showDel = false
        if (this.delete) {
          this.deleteSelected()
          return
        }
      }
      this.step = 3
      // 将移动块放回之前选中的图现在所在的位置
      let seat = seatsArray[positionArray[this.selected]]
      animationMove.translate(seat[0], seat[1]).scale(1).opacity(1).step({
        duration: 100
      })
      this.setData({
        animationMove: animationMove.export()
      })
    },

    /**
     * 相交事件触发时候，整体的移动
     * isFront true为往前移动，false为往后移动
     * startPos 需要移动的起始位置
     * endPos 需要移动的重点位置
     */
    _moveImages(isFront, startPos, endPos) {
      let length = this.data.imageArray.length
      for (let i = 0; i < length; i++) {
        let targetPos
        let position = positionArray[i]
        if (isFront) {
          // 往前移动的时候，选中图当前位置到选中图本来位置之间的图，均向后移动一格
          if (position >= startPos && position < endPos) {
            targetPos = position + 1
            positionArray[i] = targetPos
            animationArray[i].translate(seatsArray[targetPos][0], seatsArray[targetPos][1]).step()
            this.data.animationArray[i] = animationArray[i].export()
          }
        } else {
          // 往后移动的时候，选中图当前位置到选中图本来位置之间的图，均向前移动一格
          if (position > startPos && position <= endPos) {
            targetPos = position - 1
            positionArray[i] = targetPos
            animationArray[i].translate(seatsArray[targetPos][0], seatsArray[targetPos][1]).step()
            this.data.animationArray[i] = animationArray[i].export()
          }
        }
      }

      // 移动当前选中图的位置，往前的时候，移动到起点位置；往后的时候，移动到终点位置
      if (isFront) {
        positionArray[this.selected] = startPos
        animationArray[this.selected].translate(seatsArray[startPos][0] + 1, seatsArray[startPos][1]).step()
      } else {
        positionArray[this.selected] = endPos
        animationArray[this.selected].translate(seatsArray[endPos][0] + 1, seatsArray[endPos][1]).step()
      }
      this.data.animationArray[this.selected] = animationArray[this.selected].export()
      this.setData({
        animationArray: this.data.animationArray
      })
    },
    /**
       * 添加图片
       */
    addImage() {
      wx.chooseImage({
        count: 9 - this.data.imageArray.length, //最多9张图
        sizeType: ['compressed'],
        sourceType: ['album', 'camera'],
        success: (res) => {
          let length = this.data.imageArray.length
          // TODO 这里本来不必和删除的时候一样整理整个图片列表的，但是有个顺序问题，一直没处理好。。
          let newImageArray = []
          for (let i = 0; i < length; i++) {
            let position = positionArray[i]
            newImageArray[position] = this.data.imageArray[i]
          }
          newImageArray = newImageArray.concat(res.tempFilePaths)
          observerArray.forEach(item => {
            item.disconnect()
          })


          let _height;
          if (newImageArray.length % 3 == 0 && newImageArray.length < 9) {
            _height = (Math.ceil(newImageArray.length / 3) + 1) * 240 + 40;
          } else {
            _height = Math.ceil(newImageArray.length / 3) * 240 + 40;
          }

          this.setData({
            imageArray: newImageArray,
            seatNumber: newImageArray.length + 1,
            height: _height
          }, () => {
            this._addObservers()
            this._getSeats()
          })
        },
      })
    },
    /**
     * 删除当前选中的图
     */
    deleteSelected() {
      this.changeOpacity = this.data.imageArray.length == 9
      this.setData({
        selectedImg: "",
      }, () => {
        let selectedPos = positionArray[this.selected]
        let length = this.data.imageArray.length
        let newImageArray = []
        for (let i = 0; i < length; i++) {
          let position = positionArray[i]
          if (position < selectedPos) {
            newImageArray[position] = this.data.imageArray[i]
          } else if (position > selectedPos) {
            newImageArray[position - 1] = this.data.imageArray[i]
          }
        }
        observerArray.forEach(item => {
          item.disconnect()
        })
        animationArray[this.selected].opacity(1).step({
          duration: 10,
          timingFunction: 'step-end'
        })
        this.data.animationArray[this.selected] = animationArray[this.selected].export()

        let _height;
        if (newImageArray.length % 3 == 0 && newImageArray.length < 9) {
          _height = (Math.ceil(newImageArray.length / 3) + 1) * 240 + 40;
        } else {
          _height = Math.ceil(newImageArray.length / 3) * 240 + 40;
        }

        this.setData({
          imageArray: newImageArray,
          seatNumber: newImageArray.length + 1,
          height: _height,
          animationArray: this.data.animationArray
        }, () => {

          this._addObservers()
          this._getSeats()

          // 复位移动块状态
          animationMove.scale(1).opacity(1).step({
            duration: 10,
            timingFunction: 'step-end'
          })
          this.setData({
            animationMove: animationMove.export()
          }, () => {
            this.selected = -1
          })
        })
      })
    },

    /**
     * 获取最后的图片列表
     */
    getResult() {
      let length = this.data.imageArray.length
      let newImageArray = []
      for (let i = 0; i < length; i++) {
        let position = positionArray[i]
        newImageArray[position] = this.data.imageArray[i]
      }
      // console.log(newImageArray)
      return newImageArray;
    },

    /**
     * 添加相交监听
     */
    _addObservers: function () {
      let length = this.data.imageArray.length
      for (let i = 0; i < length; i++) {
        let _observer = wx.createIntersectionObserver()
        _observer
          .relativeTo('#movingCover')
          .observe('#inner' + i, (res) => {
            if (this.selected != -1 && res.intersectionRatio > 0 && res.id != ("inner" + positionArray[this.selected])) {
              let currentPos = positionArray[this.selected]
              if (i < currentPos) {
                // 往前挪，i位置到currentPos之间的所有块都向后挪1格，current == i
                this._moveImages(true, i, currentPos)
              } else if (i > currentPos) {
                // 往后挪，i位置到currentPos之间的所有块都向前挪1格，current == i
                this._moveImages(false, currentPos, i)
              }
            }
          })
        observerArray[i] = _observer
        positionArray[i] = i
      }
    },

    /**
     * 获取所有座位位置
     */
    _getSeats: function () {
      wx.createSelectorQuery().selectAll('.seats').boundingClientRect(res => {
        console.log(wx.createSelectorQuery().selectAll('.seats'))
        // 由于会有布局，要以左上角第一个位置为基准，减去一个偏移量
        let drift = [res[0].left, res[0].top]
        let length = res.length
        seatsArray = []
        animationArray = []
        this.data.animationArray = []
        for (let i = 0; i < length; i++) {
          let seat = [res[i].left - drift[0], res[i].top - drift[1]]
          seatsArray.push(seat)
          if (i < length - 1) {
            let animation = wx.createAnimation({
              timingFunction: 'easy-out'
            })
            animationArray[i] = animation
            animation.translate(seat[0], seat[1]).step({
              duration: 10,
              timingFunction: 'step-end'
            })
            this.data.animationArray[i] = animation.export()
          }
        }
        animationAdd.translate(res[length - 1].left - drift[0], res[length - 1].top - drift[1])
        if (this.data.imageArray.length == 9) {
          animationAdd.opacity(0).step({
            duration: 100,
            timingFunction: 'step-end'
          })
        } else if (this.changeOpacity) {
          animationAdd.opacity(1).step({
            duration: 100,
            timingFunction: 'step-end'
          })
          this.changeOpacity = false
        } else {
          animationAdd.step({
            duration: 100,
            timingFunction: 'step-end'
          })
        }
        this.setData({
          animationArray: this.data.animationArray,
          animationAdd: animationAdd.export()
        })
      }).exec()
    }
  }
})
