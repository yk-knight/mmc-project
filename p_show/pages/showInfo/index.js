const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
let pages = utils.initPage();
let _photosId = ""
let _cid = ""
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseInfo:{},
    myCid:"",
    commentList:[],
    sendReady:false,
    sendText:"",
    isDisabled:true,
    isTouching: false
  },
  // 点赞
  getLike(e) {
    let that = this;
    let _cosId = wx.getStorageSync('sessionId')
    let _pid = e.currentTarget.dataset.pid;
    let _isLike = this.data.baseInfo.IsLike;
    let _likeCountNum = this.data.baseInfo.LikesCount;

    if (_isLike) {
      _likeCountNum--
    } else {
      _likeCountNum++
    }
    wxReq.HttpRequst(false, "api/customerphoto/AddPhotoLike",
      { photoId: _pid, customerId: _cosId, isAddLikes: !_isLike }, 'POST',
      function (res) {
        that.setData({
          'baseInfo.IsLike': !_isLike,
          'baseInfo.LikesCount': _likeCountNum
        })
      })
  },
  // 收藏
  getCollect(e){
    let that = this;
    let _cosId = wx.getStorageSync('sessionId')
    let _pid = e.currentTarget.dataset.pid;
    let _isSC = this.data.baseInfo.IsSC;
    let _scCount = this.data.baseInfo.SCCount;

    if (_isSC) {
      _scCount--
    } else {
      _scCount++
    }
  
    wxReq.HttpRequst(false, "api/customerphoto/AddPhotoCollection",
      { photoId: _pid, customerId: _cosId, isAdd: !_isSC }, 'POST',
      function (res) {
        that.setData({
          'baseInfo.IsSC': !_isSC,
          'baseInfo.SCCount': _scCount
        })
      })
  },
  // 预约做同款
  orderService() {
    let _baseInfo = this.data.baseInfo;
    let _OrderInfo = _baseInfo.OrderInfo;
    console.log(_OrderInfo)
    wx.navigateTo({
      url: `/p_orderSuccess/pages/order/order?EntpId=${_OrderInfo.EntpId}&StoreId=${_OrderInfo.StoreId}&StoreName=${_OrderInfo.StoreName}&uid=${_OrderInfo.BUId}&photoId=${_baseInfo.PhotosId}`,
    })
  },
  //预约手艺人
  orderMechanic(e) {
    let _userInfo = this.data.baseInfo;
    let buid = _userInfo.BUId;
    let storeId = _userInfo.StoreId;
    if (buid == 0) {
      app.CallTel(e.currentTarget.dataset.tel)
    } else {
      wx.navigateTo({
        url: `/p_orderSuccess/pages/order/order?EntpId=${_userInfo.SAASStore.EntpId}&StoreId=${_userInfo.SAASStore.StoreId}&StoreName=${_userInfo.SAASStore.StoreName}&uid=${_userInfo.BUId}&photoId=${_userInfo.PhotosId}`,
      })
    }
    
  },
  // 回到主页
  goBackHome(){
    wx.switchTab({
      url: '/pages/showPlace/index'
    })
  },
  // 点击查看图片
  showPhoto(e) {
    let _photoList = e.currentTarget.dataset.photolist.map(function (item) {
      return item.PhotoPath;
    });
    wx.previewImage({
      current: e.currentTarget.dataset.path, // 当前显示图片的http链接
      urls: _photoList, // 需要预览的图片http链接列表
      success: function (e) {
      }
    })
  },
  // 点击输入框准备发表评论/回复
  timeToSend: function() {
    this.setData({
      sendReady:true
    })
  },
  // 回复评论
  replyComment:function(option){
    let index = option.currentTarget.dataset.backindex;
    let _rep = option.currentTarget.dataset.reply;
    wx.setStorageSync('replayData', _rep)
    
    // let replayData = JSON.stringify(_rep);
    
    // console.log(replayData)
    wx.navigateTo({
      url: `/p_show/pages/showReply/index?&index=${index}`,
    })
  },
  // photoId(相册Id) content（内容）customerId(客户Id) commentId(要回复所针对的评论Id, 直接评论相册，可为null
  // 发表评论
  sendDiscuss: function(e) {
    let that = this;
    utils.saveFormId(e.detail.formId);
    let _customerId = wx.getStorageSync("sessionId")
    let commentData = {
      photoId: this.data.baseInfo.PhotosId,
      content: this.data.sendText,
      customerId: _customerId,
      commentId:null
    }
    wxReq.HttpRequst(false,'api/customerphoto/ReceivePhoto',
      commentData,"POST",
      function(res){
        that.GetPhoteComment(commentData.photoId);
        if (res.points > 0){
          
          wx.showToast({
            title: `获得${res.points}颗虫豆`,
          })
          that.getBaseInfo(_photosId);
        }
      })
    this.setData({
      sendText: '',
      isDisabled: true
    })
  },
  // 取消回复
  cancleSend: function() {
    if (this.data.isDisabled){
      this.setData({
        sendReady: false
      })
    }
  },
  // 键盘输入事件
  creatInput: function(e) {
    let inputVal = e.detail.value;
    if(inputVal != ''){
      this.setData({
        isDisabled: false
      })
    }else{
      this.setData({
        isDisabled: true
      })
    }
    this.setData({
      sendText: e.detail.value
    })
  },
  shareShow(){
    wx.showShareMenu({
      withShareTicket: true
    })
  },
  // 获取单条图片秀评论列表
  GetPhoteComment(photosId){
    let that = this;
    wxReq.HttpRequst(false,'api/customerphoto/GetPhotoInfo',
      { photoId: photosId, p: pages},
      'POST',
      function(res){
        that.setData({
          commentList:res
        })
      })
  },
  // 获取图片秀主体详情
  getBaseInfo(photosId){
    let that = this;
    let cid = wx.getStorageSync("sessionId")
    wxReq.HttpRequst(false, 'api/customerphoto/GetPhotoBaseInfo',
      { photoId: photosId, customerId:cid },
      'POST',
      function (res) {
        console.log(res)
        that.setData({
          baseInfo:res
        })
      })
    
  },
  // 去用户主页
  toUserHome() {
    let _userInfo = this.data.baseInfo;
    let userRole = _userInfo.UserRole;
    if (userRole == 0) {
      wx.navigateTo({
        url: `/p_userHome/pages/userHome/index?customerId=${_cid}`,
      })
    } else if (userRole == 1) {
      let buid = _userInfo.BUId;
      let storeId = _userInfo.StoreId;
      if (buid == 0) {
        wx.navigateTo({
          url: `/p_authenticate/pages/authenticatedHome/index?customerId=${_cid}`
        })
      } else {
        wx.navigateTo({
          url: `/p_designHome/pages/designerHome/index?customerId=${_cid}&uid=${buid}&storeId=${storeId}`
        })
      }
    }
  },
  // 捕捉页面内得转发事件
  onShareAppMessage(res) {
    let that = this;
    let sharePara = {
      title: '虫虫趣美斗秀场',
    }
    wxReq.HttpRequst(false, 'api/Customer/CustomerShare',
      { customerId: that.data.myCid, shareType: 4, dataId: _cid, },
      'POST',
      function (result) {

      })
    return sharePara;
  },
  // 统计单个帖子浏览次数
  PhotoViewTimes: function () {
    wxReq.HttpRequst(false, 'api/CustomerPhoto/AddPhotoViewTimes',
      { photoId: _photosId },
      'POST',
      function (res) {
      })
  },
  // 删除我的评论
  _delComment(e){
    let that = this;
    let _id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '',
      content: '要删除这条评论嘛？',
      success(res) {
        if (res.confirm) {
          wxReq.HttpRequst(false, '/api/customerphoto/DeletePhotoCommand',
            { commentId: _id, customerId: that.data.myCid },
            'POST',
            function (res) {
              that.getBaseInfo(_photosId);
              that.GetPhoteComment(_photosId);
            })
        }
      }
    })
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _photosId = options.photosId;
    _cid = options.customerId;
    
    this.getBaseInfo(_photosId);
    this.setData({
      myCid: wx.getStorageSync("sessionId")
    })
    // this.GetPhoteComment(_photosId);
  },
  onShow(){
    if(_photosId != ""){
      this.GetPhoteComment(_photosId);
    }
    
    this.PhotoViewTimes();
  },

  touchstart(e) {
    this.setData({
      isTouching: true
    })
  },
  touchend(e){
    this.setData({
      isTouching: false
    })
  }

})

