const app = getApp();
const wxReq = require("../../../service/request.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs:['储值金','赠送金','周期/次卡'],
    tabIndex:0,
    cardBaseInfo:{},
    cardInfo:{}
  },
  // 切换tab切
  tabChange(e) {
    let _index = e.currentTarget.dataset.index;

    this.setData({
      tabIndex: _index
    })
  },
  backHome(){
    wx.switchTab({
      url:'/pages/showPlace/index'
    })
  },
  // 优惠充值
  getRecharge(e){
    let _tabIndex = this.data.tabIndex;
    console.log(e)
    let _BaseMoney = this.data.cardBaseInfo.BaseMoney;
 
    wx.navigateTo({
      url: `/p_card/pages/cardPayment/index?BaseMoney=${_BaseMoney ? _BaseMoney : 0}&storeId=${this.data.cardBaseInfo.ShopId}&tabindex=${_tabIndex == 0?_tabIndex:1}`,
    })
  },
  getCardInfo(_cardId){
    let that = this;
    wxReq.HttpRequst(false, 'api/customercard/GetCardInfo',
      { cardId: _cardId },
      'POST',
      function (res) {
        
        let { Account1, Account2, Account3 } = res
        that.setData({
          cardBaseInfo:res.card,
          cardInfo: { Account1, Account2, Account3 }
        })
        console.log(that.data.cardInfo)
      }
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.getCardInfo(options.cardId);

  }
})