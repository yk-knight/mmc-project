// 会员卡充值
const wxReq = require("../../../service/request.js")
var app = getApp();
let sCid = ""
let sDisCountId = ""
let sCycleCardId = ""
let sPrice = 0
let iStoreId = null
let cardName = ""
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StoreDiscount: [],
    StoreCard: [],
    BaseMoney: "",
    tabs: ['余额充值', '卡内充值'],
    tabIndex: 0,
    disIndex: -1,
    cardIndex: -1
  },
  // 充值额度渲染
  _getStoreDiscount(options) {
    let that = this;
    wxReq.HttpRequst(false, 'api/customercard/GetStoreDiscount', { storeId: options.storeId }, 'POST', (result) => {
      that.setData({
        StoreDiscount: result
      })
    })
  },
  // 次卡/周期卡渲染
  _getStoreCard(options) {
    let that = this;
    wxReq.HttpRequst(false, 'api/customercard/GetStoreCycleCard', { storeId: options.storeId }, 'POST', (result) => {
      that.setData({
        StoreCard: result
      })
    })
  },

  // 切换tab
  _changeTabs(e) {
    this.setData({
      tabIndex: e.detail,
      disIndex: -1,
      cardIndex: -1
    })
    sDisCountId = ""
    sCycleCardId = ""
    sPrice = 0
  },

  // 选择充值金额
  checkPrice(e) {

    let _price = e.currentTarget.dataset;
    this.setData({
      disIndex: _price.index
    })

    sDisCountId = _price.id
    sPrice = _price.price

  },

  // 选择次卡
  checkCard(e) {

    let _price = e.currentTarget.dataset;
    this.setData({
      cardIndex: _price.index
    })
    cardName = _price.name
    sCycleCardId = _price.id
    sPrice = _price.price

  },

  // 预约充值接口调用，获取F码
  GetFcode(url, data) {
    wxReq.HttpRequst(false,
      url, data, 'POST', (result) => {
        let _data = JSON.stringify(result);
        wx.navigateTo({
          url: `/p_fCode/pages/fCode/index?cardName=${cardName}&tabIndex=${this.data.tabIndex}&data=${_data}&price=${sPrice}`,
        })
      })
  },
  ErrorMsg(msg) {
    wx.showToast({
      title: msg,
      icon: 'none'
    })
  },
  // 确认预约充值
  paySubmit() {
    let _type = this.data.tabIndex;
    let _url = "";
    let _data = {
      customerId: sCid,
      storeId: iStoreId
    }
    if (_type == 0) {
      if (sDisCountId != "") {
        _url = "api/customercard/BuyRechargeCard"
        _data.disCountId = sDisCountId
      } else {
        this.ErrorMsg("请选择充值金额");
        return;
      }
    } else {
      if (sCycleCardId != "") {
        _url = "api/customercard/BuyCycleCard"
        _data.cycleCardId = sCycleCardId
      } else {
        this.ErrorMsg("请选择充值卡类型");
        return;
      }
    }
    this.GetFcode(_url, _data)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    iStoreId = options.storeId;
    this.setData({
      BaseMoney: options.BaseMoney,
      tabIndex: parseInt(options.tabindex)
    })
    this._getStoreDiscount(options)
    this._getStoreCard(options)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    sCid = wx.getStorageSync("sessionId")
  },

})