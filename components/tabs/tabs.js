// components/tabs/tabs.js
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  externalClasses: ['my-tabs'],
  /**
   * 组件的属性列表
   */
  properties: {
    tabs:{
      type:Array,
      value: ['最新动态', '预约历史']
    },
    tabIndex:{
      type:Number,
      value:0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // tabIndex:0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 切换tab切
    tabChange(e) {
      
      let _index = e.currentTarget.dataset.index;
      // let isEmpty = this.SearchOrderList(_index);

      this.setData({
        tabIndex: _index
      })
      this.triggerEvent('changeTabs', _index)
    },

  }
})
