// pages/myOrder/index.js
const app = getApp();
const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
var p = {
  PageIndex: 0,
  PageSize: 10
}
let _type = 0;
let _month = 0;
let _list = [];

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: false,
    tabs: ['最新动态', '预约历史'],
    month: [{
      id: 0,
      name: '全部'
    }, {
      id: 3,
      name: '近三个月'
    }, {
      id: 6,
      name: '最近半年'
    }, {
      id: 12,
      name: '最近一年'
    }],
    mIndex: 0,
    listEnd: false,
    tabIndex: 0,
    appStatus: true,
    customerId: wx.getStorageSync('sessionId'),
    orderList: [],
    noData: false
  },
  // 再次预约
  orderAgain(e) {
    let _order = e.currentTarget.dataset.order;
    wx.navigateTo({
      url: `/p_orderSuccess/pages/order/order?EntpId=${_order.EntpId}&StoreId=${_order.StoreId}&StoreName=${_order.StoreName}`,
    })
  },
  // 切换查看时间筛选
  getMonthChange(e) {
    let _data = e.currentTarget.dataset
    _month = _data.value;
    this.setData({
      mIndex: _data.index
    })
    this.getOrderList(p)
  },
  // 切换缺省图显示
  getEmptyIco(empty) {
    this.setData({
      noData: empty
    })
  },
  // 切换tab切
  tabChange(e) {
    let _index = e.detail
    let isEmpty = this.SearchOrderList(_index);

    if (_index == _type) { return }

    p.PageIndex = 0

    _index == 0 ? _type = 0 : _type = -1


    this.setData({
      tabIndex: _index,
      orderList: []
    })
    this.getOrderList(p)

  },
  // 取消预约
  cancleOrder(e) {
    let orderId = e.currentTarget.dataset.orderid;
    wx.navigateTo({
      url: `/p_cancelOrder/pages/cancelOrder/index?orderId=${orderId}`,
    })
  },
  // 遍历数组
  SearchOrderList(tab) {

    let _array = this.data.orderList;

    return _array.some(function (val) {
      if (tab < 1) {
        return val.AppointmentStatus < 1;
      } else {
        return val.AppointmentStatus > 0;
      }
    })
  },
  // 我的历史预约记录
  getOrderList(pages) {
    let that = this;
    let _customerId = wx.getStorageSync('sessionId');
    wxReq.HttpRequst(false,
      'api/Customer/GetMyAppointment',
      { customerId: _customerId, month: _month, type: _type, p: pages },
      'POST',
      (res) => {
        console.log(res.AppointmentList)
        let _length = res.AppointmentList.length

        if (pages.PageIndex == 0){
            if(_length < 1){
              that.getEmptyIco(true);
              that.setData({
                orderList: []
              })
            }else{
              that.getEmptyIco(false);
              that.setData({
                orderList: res.AppointmentList
              })
            }
            that.setData({
              listEnd: false
            })
            return;
        }else{
            let _appList = that.data.orderList
              _appList = _appList.concat(res.AppointmentList)

          that.getEmptyIco(false);
          that.setData({
            orderList: _appList
          })

          if (_length < 10) {
            that.setData({
              listEnd: true
            })
          } else {
            that.setData({
              listEnd: false
            })
          }

        }
      })
  },
  onShow: function () {
    _type = 0;
    this.getOrderList(p);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onLoad: function () {
    // _type = 0;
    // this.getOrderList(p);
  },

  // 监听用户上拉触底事件
  onReachBottom: function() {
    if (!this.data.listEnd){
      p.PageIndex += 1;
      this.getOrderList(p)
    }

  }

})