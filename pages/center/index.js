const wxReq = require("../../service/request.js");
var app = getApp();
let _customer = {};
let _customerId=""
Page({
  data:{
    newmsg:0,
    newmsg_Command:0,
    newphoto:0,
    city:"南京",
    shortcut:[
      {
        name:'关注',
        imgUrl:'../imgs/centerInit-01.png',
        url:'/p_center/pages/myFollow/index?newphoto=',
      },
      {
        name: '消息',
        imgUrl: '../imgs/centerInit-02.png',
        url:'/p_center/pages/message/index'
      },
      {
        name: '收藏',
        imgUrl: '../imgs/centerInit-03.png',
        url:'/p_collect/pages/collect/index'
      },
      {
        name: '预约',
        imgUrl: '../imgs/centerInit-04.png',
        url:'/p_center/pages/myOrder/index'
      },
      {
        name: '会员卡',
        imgUrl: '../imgs/centerInit-05.png',
        url:'/p_center/pages/myCard/index'
      },
    ],
    customer:{},
    coupons:0,
    appointment:[],
    userTags:[],
    isPopHide: true,
    guideData: [
      {
        id: 0,
        guideImg: 'https://www.mmcmy.com/commonimg/ADImage/commen/4.png',
        guideText: '我已了解',
        isNav: false
      },
    ]
  },
  //获取当前所在城市
  getUserLocation: function () {
    let that = this;
    if (getCity) {
      return;
    } else {
      getCity = true;
    }
    qqMap.reverseGeocoder({
      success: function (e) {
        let _locationCity = e.result.address_component.city;
        let _newCity = _locationCity.slice(0, _locationCity.length - 1); //当前城市
        this.setData({
          city: _newCity
        })

      }
    });
  },
  // 去我的主页
  setFullInfo(){
    let userRole = this.data.customer.UserRole
    if (userRole == 0){
      wx.navigateTo({
        url: `/p_userHome/pages/userHome/index`
      })
    } else if (userRole == 1){
      let buid = this.data.customer.BUId;
      let storeId = this.data.customer.StoreId;
      if (buid == 0){
        wx.navigateTo({
          url: `/p_authenticate/pages/authenticatedHome/index`
        })
      }else{
        wx.navigateTo({
          url: `/p_designHome/pages/designerHome/index?uid=${buid}&storeId=${storeId}`
        })
      }
    }
  },
  // 查看订单列表
  toMyOrder(e){

    let _tab = e.currentTarget.dataset.tab
    wx.navigateTo({
      url: `/p_history/pages/myHistory/index?tabid=${_tab}`,
    })
  },
  // 虫豆商城
  toMall(){
    this.setData({
      isPopHide: false
    })
  },
  onLoad(){
    let _city = wx.getStorageSync("city");
    if(_city == ""){
      this.getUserLocation
    }else{
      this.setData({
        city: _city
      })
    }
    
  },
  onShow:function(){
    _customerId = wx.getStorageSync('sessionId');
    this.GetMyInfo();
    this.GetNewMsg()
  },
  // 获取最新消息
  GetNewMsg() {
    let that = this
    wxReq.HttpRequst(false, 'api/Customer/MyMsgTip',
      { customerId: _customerId },
      'POST',
      function (result) {
        that.setData({
          newmsg_Command: result.newmsg_Command,
          newmsg: result.newmsg,
          newphoto: result.newphoto
        });
				if (result.newmsg == 0 && result.newmsg_Command == 0) {
					wx.removeTabBarBadge({
						index: 2
					})
					if (result.newphoto > 0) {
						wx.showTabBarRedDot({
							index: 2
						})
					} else {
						wx.hideTabBarRedDot({
							index: 2
						})
					}
				} else {
					wx.setTabBarBadge({
						index: 2,
						text: (result.newmsg + result.newmsg_Command) + ""
					})
				}
      })
  },
  GetMyInfo(){
    let that = this;
    wxReq.HttpRequst(false, 'api/Customer/MyHome', { customerId: _customerId }, 'POST', 
      function(result) {
        let _userTags = []
        let _labelContent = result.customer.LabelContent
        app.globalData.labelContent = _labelContent;
        
        if(_labelContent){
          _userTags = _labelContent.split(',')
          that.setData({
            userTags: _userTags
          });
        }
        if (_customer !== result.customer){
          that.setData({
            customer: result.customer,
            appointment: result.appointment,
            coupons: result.Coupons
          });
          _customer = result.customer;
        }
        
      }
    )
  }
})