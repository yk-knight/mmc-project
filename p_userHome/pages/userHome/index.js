const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
let pages = utils.initPage();
let app = getApp();
let _customerId = "";
let updateInfo = 0;
let startX = 0; //开始坐标
let startY = 0;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userBaseInfo:{},
    photoList:[],
    isAttention:false,
    myCustomerId:'' 
  },
  // 分享转发
  onShareAppMessage(){
    return {

      title: this.data.userBaseInfo.customer.NickName+'的个人主页',

      desc: '分享页面的内容',

      path: `/p_userHome/pages/userHome/index?customerId=${_customerId}` // 路径，传递参数到指定页面。

    }
  },
  // 我的斗秀场个人信息
  getMyInfo(){
    let that = this;

    wxReq.HttpRequst(false,'api/customerphoto/MyPhotoBase',
      { customerId: _customerId, myselfId: that.data.myCustomerId},
      'POST',function(res){
        wx.setNavigationBarTitle({
          title: res.customer.NickName//页面标题为路由参数
        })
        let _baseInfo = res;
        // 计算星座
        if (_baseInfo.customer.Birthday){
          let _date = _baseInfo.customer.Birthday.split(" ")[0];
          _baseInfo.customer.constellation = app.GetConstellation(_date)
        }
        that.setData({
          userBaseInfo: _baseInfo
        })
        updateInfo = 0;
      })
  },
  // 我的秀场帖子列表
  getMyShow(){
    let that = this;

    wxReq.HttpRequst(false, 'api/customerphoto/MyPhotoList',
      { customerId: _customerId, myselfId: that.data.myCustomerId, p: pages },
      'POST', function (res) {
        that.setData({
          photoList: res
        })
      })
  },
  // 长按删除帖子
  longTap(e){
    let that = this;
    let _pid = e.currentTarget.dataset.pid;

    let thisCid = wx.getStorageSync('sessionId');
    if (thisCid != _customerId){
      return;
    }
    wx.showModal({
      title:'',
      content:'是否删除？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '正在删除...'
          });
          wxReq.HttpRequst(false, 'api/customerphoto/DeletePhoto',
            { customerId: _customerId, photoId:_pid},
            'POST', function (res) {
              wx.hideLoading()
              that.getMyShow()
            })
        } else if (res.cancel) {
        }
      }
    })
  },
  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    let thisCid = wx.getStorageSync('sessionId');
    if (thisCid != _customerId) {
      return;
    }
    var newObj = { "isTouchMove" : false};
    var newArr = new Array();
    this.data.photoList.forEach(function (v, i) {
      // if (!v[isTouchMove])//只操作为true的
      //   v[isTouchMove] = false;
      v = {
        ...v,
        ...newObj
      }
      newArr.push(v);
    })
    startX = e.changedTouches[0].clientX;
    startY = e.changedTouches[0].clientY;
    this.setData({
      photoList: newArr
    })
  },
  //滑动事件处理
  touchend: function (e) {
    var that = this,
      pId = e.currentTarget.dataset.pid,//当前索引
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });

      that.data.photoList.forEach(function (v, i) {
        v.isTouchMove = false
        //滑动超过30度角 return
        if (Math.abs(angle) > 30) return;
        if (v.PhotosId == pId) {
          if (touchMoveX > startX) //右滑
            v.isTouchMove = false
          else if ((startX - touchMoveX) >10) //左滑
            v.isTouchMove = true
        }
      })
    //更新数据
    that.setData({
      photoList: that.data.photoList
    })
  },
  /**
   * 计算滑动角度
   * @param {Object} start 起点坐标
   * @param {Object} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //删除事件
  del: function (e) {

  },
  // 点击查看图片
  showPhoto(e) {
    let _photoList = e.currentTarget.dataset.photolist.map(function (item) {
      return item.PhotoPath;
    });
    wx.previewImage({
      current: e.currentTarget.dataset.path, // 当前显示图片的http链接
      urls: _photoList, // 需要预览的图片http链接列表
      success: function (e) {
      }
    })
  },
  // 关注虫虫用户
  getAttention(event){
    let that = this;
    let _cusId = wx.getStorageSync('sessionId');
    let _attention = !this.data.userBaseInfo.customer.IsLike
    wxReq.HttpRequst(false, 'api/Customer/AttentionAction',
      { customerId: _cusId, dataid: _customerId, AttentionType: 3, optType: _attention?0:1 },
      'POST', function (res) {
        that.setData({
          'userBaseInfo.customer.IsLike': _attention
        })
      })
  },
  // 点赞
  getLike(e) {
    let _index = e.currentTarget.dataset.index;
    let _pid = e.currentTarget.dataset.pid;
    let _cosId = this.data.myCustomerId;
    let _isLike = 'photoList[' + _index + '].IsLike';
    let _likeCount = 'photoList[' + _index + '].LikesCount';
    let _likeCountNum = this.data.photoList[_index].LikesCount;
    let _isAddLike = this.data.photoList[_index].IsLike;
    if (_isAddLike) {
      this.setData({
        [_isLike]: !_isAddLike,
        [_likeCount]: _likeCountNum - 1
      })
    } else {
      this.setData({
        [_isLike]: !_isAddLike,
        [_likeCount]: _likeCountNum + 1
      })
    }
    wxReq.HttpRequst(false, "api/customerphoto/AddPhotoLike",
      { photoId: _pid, customerId: _cosId, isAddLikes: !_isAddLike }, 'POST',
      function (res) {

      })
  },
  // 进入帖子详情
  getToInfo(event){
    let _pid = event.currentTarget.dataset.photoid;
    wx.navigateTo({
      url: `/p_show/pages/showInfo/index?photosId=${_pid}`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let myCustomerId = wx.getStorageSync('sessionId');
    this.setData({
      myCustomerId: myCustomerId
    })
    var arr = Object.keys(options);

    if(arr.length == 0){
      _customerId = myCustomerId
    }else{
      _customerId = options.customerId
    }

    if (updateInfo == 0) {
      updateInfo = 1;
      this.getMyInfo();
    }
    this.getMyShow();
      
  },
  // 统计浏览量
  setSiteViewCount(){
    wxReq.HttpRequst(false, "api/customerphoto/AddViewTimes",
      { customerId: _customerId }, 'POST',
      function (res) {
      })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.myCustomerId != _customerId) {
      this.setSiteViewCount();
    } 
    if (updateInfo == 0) {
      updateInfo = 1;
      this.getMyInfo();
    }
  }
})