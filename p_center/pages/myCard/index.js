// pages/myCard/index.js
const app = getApp();
const wxReq = require("../../../service/request.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardList:[],
    nodata:false
  },
  showDetail: function(data){
    console.log(data)
    let _info = data.currentTarget.dataset.cardinfo;
    let _cardInfo = JSON.stringify(_info);
    wx.navigateTo({
      url: `/p_card/pages/cardInfo/index?cardId=${_info.CardId}`,
    })
  },
  getCardList: function (_customerId){
    let that = this;
    wxReq.HttpRequst(false, 'api/customercard/MyCards',
      { customerId: _customerId },
      'POST',
      function (res) {
        if(res.length < 1){
          that.setData({
            nodata:true
          })
        }else{
          that.setData({
            nodata: false,
            cardList: res
          })
        }
        
      }
    )
  },
  // 去门店
  toStores(){
    wx.switchTab({
      url: '/pages/index/index'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let sId = wx.getStorageSync("sessionId")
    this.getCardList(sId);
  }
})