let orderId = ""
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },
  toShowPlace(){
    wx.redirectTo({
      url: `/p_showCreat/pages/showCreate/index?orderId=${orderId}`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    orderId = options.orderId;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})