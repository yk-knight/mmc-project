const wxReq = require("../../../service/request.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    autoFocus:true,
    adjustPosition:true,
    maxlength:500,
    array: [
      {code:0,value:'交互体验不好'},
      {code:1,value:'使用卡顿'}, 
      {code:2,value:'功能不能满足需求'},
      {code:3,value:'需求建议'},
      {code:99,value:'其他'}
    ],
    index: -1,
  },
  bindPickerChange(e) {

    this.setData({
      index: e.detail.value
    })
  },
  // 提交反馈
  formSubmit(e){

    let data = e.detail.value;
    let cid = wx.getStorageSync("sessionId")

    if (data.diccode != null){
      wxReq.HttpRequst(false, 'api/mmc/SubmitSuggest',
        { customerId: cid, diccode: data.diccode, content: data.content, tel: data.tel }, 'POST',
        (result) => {
          wx.showToast({
            title: '提交成功，谢谢您的宝贵意见！',
            duration: 2000,
            success: () => {
              setTimeout(function () {
                wx.navigateBack({
                  id: 1
                })
              }, 3000)

            }
          })
        }) 
    }else{
      wx.showToast({
        title: '亲，您的意见还没填完哦！',
        icon:'none'
      })
    }
    

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  }
})