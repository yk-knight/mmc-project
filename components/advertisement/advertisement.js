// components/advertisement/advertisement.js
Component({
  /**
   * 组件的属性列表
   */

  externalClasses: ['my-ad-wrap'],

  properties: {
    // isHide: {
    //   type: Boolean,
    //   value: false
    // }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isHide: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    closeAD() {
      this.setData({
        isHide: true
      })
    }
  }
})
