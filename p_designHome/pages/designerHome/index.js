const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js");
const app = getApp();
let pages = utils.initPage();
let realName = "";
let uid =  0;
let sid = 0;
let newDesign={};
let newProduct = []
let _customerId = "";  //被查看主页的用户的
let _myCustomerId = ""; //当前用户的
let _option = {};
let _top = 0;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    design:{},
    tabs: ['顾客晒照', '擅长作品'],
    tabIndex:0,
    isFollow: false,
    product:[],
    orderPhoto:[],
    fixedNav:false,
    myCustomerId:'',
    customerId: ''
  },
  onPageScroll: function(e){
    
    let _fexed = false;
    if (!this.data.fixedNav) {  //临界值，根据自己的需求来调整
      if (e.scrollTop >= _top){
        this.setData({
          fixedNav: true,    //是否固定导航栏
        })
      }
      
    } else {

      if (e.scrollTop < _top){
        this.setData({
          fixedNav: false,    //是否固定导航栏
        })
      }
      
    }

  },
  toShowInfo(){
    wx.navigateTo({
      url: '/pages/showInfo/index',
    })
  },
  showUserPhoto(e){
    let _path = e.currentTarget.dataset.path
    wx.previewImage({
      urls:[_path]
    })
  },
  // 技师主页分享
  onShareAppMessage(res) {
    let _rName = this.data.design.user.realName;
    let sharePara = {
      title: _rName + "的主页",
      path: `/p_designHome/pages/designerHome/index?uid=${uid}&storeId=${sid}`
    }
    wxReq.HttpRequst(false, 'api/Customer/CustomerShare',
      { customerId: _myCustomerId, shareType: 2, dataId: uid, },
      'POST',
      function (result) {

      })
    return sharePara;
  },
  // 点击查看图片
  showPhoto(e) {

    let _photoList = e.currentTarget.dataset.photolist.map(function (item) {
      return item.PhotoPath;
    });

    wx.previewImage({
      current: e.currentTarget.dataset.path, // 当前显示图片的http链接
      urls: _photoList, // 需要预览的图片http链接列表
      success: function (e) {

      }
    })
  },
  // 监听tab切换
  _changeTabs(e) {
    this.setData({
      tabIndex: e.detail
    })

  },
  // 关注技师
  getFollow(){
    let _isAtten = this.data.design.user.IsLike;
    let _uid = parseInt(uid)
    if(!_isAtten){
      app.SetAttention(_uid, 2, 0)
        .then((res) => {
          this.setData({
            'design.user.IsLike': true
          });
          // 等待关注请求发送完毕
          this.getDisgnerInfo(_option)
        });
      
    }else{
      app.SetAttention(_uid, 2, 1)
        .then((res) => {
          this.setData({
            'design.user.IsLike': false
          });
          // 等待关注请求发送完毕
          this.getDisgnerInfo(_option)
        });
    }
    
  },
  // 获取技师档案
  getDisgnerInfo(option){
    let that = this
    wxReq.HttpRequst(false, 'api/storeuser/GetStoreUserBase',
      { buid: option.uid, storeId: option.storeId, customerId: _myCustomerId }, 'POST',
      function (result) {
        console.log(result)
        if (result === newDesign){
          return;
        }
        wx.setNavigationBarTitle({
          title: result.user.RealName + "的主页"//页面标题为路由参数
        })
        that.setData({
          design: result
        })
        newDesign = result;

        if (result.bindCustomer){
          that.setData({
            customerId: result.bindCustomer.CustomerId
          })
        }
      }
    )
  },
  // 获取技师作品集
  // getDisgnerPics(sid){
  //   let that = this;
  //   wxReq.HttpRequst(false, 'api/storeuser/GetStoreUserAffix',
  //     { buid: uid, storeId: sid, p: pages }, 'POST',
  //     function (result) {
  //       if (result === newDesign) {
  //         return;
  //       }
  //       that.setData({
  //         product:result
  //       })
  //       newProduct = result;
  //     }
  //   )
  // },
  // 获取技师擅长作品
  getDesignPhoto() {
    let that = this;
    wxReq.HttpRequst(false, "api/customerphoto/GetPhotoList",
      { customerId: _myCustomerId, p: pages, viewFlag: 0, buid: uid, butype: 1 }, 'POST',
      function (res) {
        let newRes = [];
        res.forEach((v,i)=>{
          if (!v.OrderInfo){
            newRes.push(v)
          }
        })
        console.log(newRes)
        that.setData({
          product: newRes
        })
        newProduct = res;
      })
  },
  // 获取技师服务作品（来源斗秀场）
  getServerPhoto(){
    console.log(this.data.design)
    let that = this;
    // let _uid = this.data.design.user.UID
    wxReq.HttpRequst(false, "api/customerphoto/GetPhotoList",
      { customerId: _myCustomerId, p: pages, viewFlag: 0, buid: uid,}, 'POST',
      function (res) {
        // console.log(res)
        that.setData({
          orderPhoto:res
        })
      })
  },
  // 预约服务
  getOrderFor() {
    let _store = this.data.design;
    wx.navigateTo({
      url: `/p_orderSuccess/pages/order/order?EntpId=${_store.store.EntpId}&StoreId=${_store.store.StoreId}&StoreName=${_store.store.StoreName}&uid=${_store.user.UID}`,
    })
  },
  onReady:function(){
    let query = wx.createSelectorQuery()
    query.select('#proPic').boundingClientRect((rect) => {
      _top = rect.top
    }).exec()
  },
  //查看帖子详情
  toPhotoDetail(e){
    let _pid = e.currentTarget.dataset.pid;
    wx.navigateTo({
      url: `/p_show/pages/showInfo/index?photosId=${_pid}&customerId=${_customerId}`
    })
  },
  // 长按删除帖子
  longTap(e) {
    let that = this;
    let _pid = e.currentTarget.dataset.pid;

    if (_myCustomerId != _customerId) {
      return;
    }
    wx.showModal({
      title: '',
      content: '是否删除？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '正在删除...'
          });
          wxReq.HttpRequst(false, 'api/customerphoto/DeletePhoto',
            { customerId: _myCustomerId, photoId: _pid },
            'POST', function (res) {
              wx.hideLoading()
              that.getDesignPhoto()
            })
        } else if (res.cancel) {
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _option = options
    _myCustomerId = wx.getStorageSync('sessionId');
    this.setData({
      myCustomerId: _myCustomerId
    })
    var arr = Object.keys(options);
    if (!arr.customerId) {
      _customerId = _myCustomerId  //自己查看自己的个人主页
    } else {
      _customerId = options.customerId //查看别人的个人主页
    }
    uid = options.uid;
    sid = options.storeId;
    
    this.getDisgnerInfo(options);
    // this.getDisgnerPics(options.storeId);
    this.getServerPhoto();
    this.getDesignPhoto();
  }
})