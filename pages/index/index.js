//index.js
//获取应用实例
const app = getApp();
const wxReq = require("../../service/request.js")
const utils = require('../../utils/util.js')
const qqMap = utils.qqmapsdk;
let savedImgUrl = ""

let _cid = '';
let _lat = 0;
let _lng = 0;
let _storeData = ""; //离线缓存
let _recData = "";  //离线缓存
let getCity = false;
Page({
  data: {
    canvasShow:false,
    userInfo: {},
    userLocation:{},
    hasUserInfo: false,
    searchCity: "南京",
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    storeType:[
      { name: '活动促销', color: 'yellow', url: 'https://www.mmcmy.com/commonimg/ADImage/commen/store_03.png',      path:'/p_storeList/pages/storeList/index?viewcode=06'},
      { name: '个性时尚', color: 'red', url: 'https://www.mmcmy.com/commonimg/ADImage/commen/store_05.png', path:'/p_storeList/pages/storeList/index?viewcode=03' }, 
      { name: '快美工厂', color: 'blue', url: 'https://www.mmcmy.com/commonimg/ADImage/commen/store_09.png', path:'/p_storeList/pages/storeList/index?viewcode=04' }, 
      { name: '高端体验', color: 'green', url: 'https://www.mmcmy.com/commonimg/ADImage/commen/store_10.png', path: '/p_storeList/pages/storeList/index?viewcode=05' }
    ],
    bannerList: [{}],
    // newStoreList: [{},{},{}],
    recommendStoreList:[],
    // recentStore:[],最近光顾门店
    indicatorDots: false,
    vertical: false,
    autoplay: false,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 10,
    nextMargin: 10,
    imgheights: [],
    //图片宽度 
    imgwidth: 750,
    //默认  
    current: 0
  },
  //这是一个封装好的方法(生成分享图片)  
  promisify: api => {
    return (options, ...params) => {
      return new Promise((resolve, reject) => {
        const extras = {
          success: resolve,
          fail: reject
        }
        api({ ...options, ...extras }, ...params)
      })
    }
  },
  // 首页banner图高度计算
  _imageLoad: function (e) {
    //获取图片真实宽度
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比
      ratio = imgwidth / imgheight;
    //计算的高度值
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight
    var imgheights = this.data.imgheights
    //把每一张图片的高度记录到数组里
    imgheights.push(imgheight)
    this.setData({
      imgheights: imgheights,
    })
  },
  // 轮播图手动切换
  bindchange: function (e) {
    this.setData({ current: e.detail.current })
  },
  //获取当前所在城市
  getUserLocation:function(){
    let that = this;
    if (getCity){
      return;
    }else{
      getCity = true;
    }
    qqMap.reverseGeocoder({
      success:function(e){
        let _locationCity = e.result.address_component.city; 
        let _newCity = _locationCity.slice(0, _locationCity.length - 1); //当前城市
        
        let _isCityChange = app.globalData.isCityChange;

        if (_newCity != "南京" || _isCityChange){
          wx.showModal({
            title: '定位到您在 ' + _newCity,
						content: '虫虫还没来到' + _newCity+"哦",
            confirmText: '知道了',
            success(res) {
              if (res.confirm) {
                that.setData({
                  searchCity: "南京"
                });
                app.globalData.city = _newCity
                wx.setStorageSync('city', _newCity);
              }
            }
          })
        }else{
          wx.setStorageSync('city', '南京');
        }
        
      }
    });
  },
  // 切换所在城市
  getCity: function(){
    wx.navigateTo({
      url: '/p_storeList/pages/chooseCity/index'
    })
  },
  // 加载离线缓存数据
  GetStorageData(){
    _storeData = wx.getStorageSync("storeData"); //读取离线缓存
    _recData = wx.getStorageSync("recData"); //读取离线缓存

    // banner，新店打榜数据
    if (_storeData && _storeData != "") {
      
      let _listData = JSON.parse(_storeData)

      this.setData({
        bannerList: _listData.bannerList,
        // newStoreList: _listData.newStoreList,
      });
    };

    //人气推荐数据
    if (_recData && _recData != ""){

      let _listData = JSON.parse(_recData)

      this.setData({
        recommendStoreList: _listData.recommendStoreList,
      });
    }
    
  },
  // 获取新店及广告banner
  getIndexInfo: function() {

    const that = this;
    wx.showLoading({
      title: '正在加载...',
    })
    wxReq.HttpRequst(false, 'api/MMC/Home', 
      { customerId: _cid, latitude: _lat, longitude:_lng },'POST',(res) => {

        let _strRes = JSON.stringify(res)

        if (_storeData !== _strRes){
          // 先判断数据是否有更新
          wx.setStorageSync("storeData", _strRes);
          if (res) {
            that.setData({
              bannerList: res.bannerList,
              // newStoreList: res.newStoreList,
            });
          }
        } 
        wx.hideLoading()
        
    })
  },

  // 获取人气推荐店铺和近期消费店铺
  getRecommendList(){
    
    const that = this;
    
    wxReq.HttpRequst(false, 'api/MMC/HomeRecommend',
      { customerId: _cid, latitude: _lat, longitude: _lng }, 'POST', (res) => {

        let _strRes = JSON.stringify(res)

        if (_recData !== _strRes){
					wx.setStorageSync("recData", _strRes);
          if (res) {
            that.setData({
              recommendStoreList: res.recommendStoreList
              // recentStore: res.myRecentStore
            });
          }

        }
        
      })
  },

  // 跳转门店首页
  toStore(e){
    const _storeData = e.currentTarget.dataset.store
    app.globalData.storeInfo = _storeData
    wx.navigateTo({
      url: `/p_store/pages/B-homePage/index?StoreId=${_storeData.StoreId}`,
    })
  },
  // 进入搜索页
  starSearch(){
    wx.navigateTo({
      url: `/p_storeList/pages/storeList/index?toSearch=${true}`,
    })
  },
  // 获取用户当前位置
  getLatLng(){
    let that = this;
    utils.getUserLocation().then(res => {
      console.log(res)
      _lat = res.latitude;
      _lng = res.longitude;

      that.setData({
        userLocation: res
      })
    }).catch(e => {
      console.log(e)
    })
  },
  onShow:function(options){
   
    _cid = wx.getStorageSync("sessionId")
    
    if (_cid != "") {
      this.GetStorageData()
      this.getUserLocation();
      this.getLatLng();
      this.getIndexInfo();
      this.getRecommendList();
    }
    const wxGetImageInfo = this.promisify(wx.getImageInfo)
    Promise.all([
      wxGetImageInfo({
        src: 'https://www.mmcmy.com/commonimg/ADImage/commen/3.png'
      }),
      wxGetImageInfo({
        src: 'https://www.mmcmy.com/commonimg/ADImage/commen/store_03.png'
      })
    ]).then(res => {
      const ctx = wx.createCanvasContext('shareCanvas')
      // 底图
      ctx.drawImage(res[0].path, 0, 0, 250, 350)
      // 作者名称
      ctx.setTextAlign('center')    // 文字居中
      ctx.setFillStyle('#000000')  // 文字颜色：黑色
      ctx.setFontSize(22)         // 文字字号：22px
      ctx.fillText('作者：Eric', 250 / 3, 50)
      // 小程序码
      const qrImgSize = 100
      ctx.drawImage(res[1].path, (250 - qrImgSize) / 2, 130, qrImgSize, qrImgSize)
      ctx.stroke()
      ctx.draw()
    })

  },
  // 初始化页面
  onLoad: function (options) {
    let that = this;
    wx.showShareMenu({
      withShareTicket: true
    })
  },
  // 分享朋友圈
  shareFgod:function(){
    this.setData({
      canvasShow: true,
    })
  },
  //保存海报
  saveImageToPhoto: function () {
    var that = this;
    setTimeout(function () {
      wx.canvasToTempFilePath({
        x: 0,
        y: 0,
        width: 250,
        height: 350,
        destWidth: 1035,
        destHeight: 1560,
        canvasId: 'shareCanvas',
        success: function (res) {
          console.log(res, '保存')
          
          savedImgUrl= res.tempFilePath

        }
      })
    }, 1000)
    setTimeout(function () {
      console.log(savedImgUrl)
      if (savedImgUrl != "") {
        console.log(savedImgUrl)
        wx.saveImageToPhotosAlbum({
          filePath: savedImgUrl,
          success: function () {
            wx.showModal({
              title: '保存图片成功',
              content: '图片已经保存到相册，快去炫耀吧！',
              showCancel: false,
              success: function (res) {
                that.setData({
                  canvasShow: false,
                })
              },
              fail: function (res) { },
              complete: function (res) { },
            });
          },
          fail: function (res) {
            console.log(res);
            if (res.errMsg == "saveImageToPhotosAlbum:fail cancel") {
              wx.showModal({
                title: '保存图片失败',
                content: '您已取消保存图片到相册！',
                showCancel: false
              });
            } else {
              wx.showModal({
                title: '提示',
                content: '保存图片失败，您可以点击确定设置获取相册权限后再尝试保存！',
                complete: function (res) {
                  console.log(res);
                  if (res.confirm) {
                    wx.openSetting({}) //打开小程序设置页面，可以设置权限
                  } else {
                    wx.showModal({
                      title: '保存图片失败',
                      content: '您已取消保存图片到相册！',
                      showCancel: false
                    });
                  }
                }
              });
            }
          }
        })
      }
    }, 1500)
  }
})
