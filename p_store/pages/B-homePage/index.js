// 门店主页
const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
var app = getApp();
let pageTitle = ""
let pageId = ""
let _customerId = ""
let _userLocation = {}
let _km = 0
let isLoad = false;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    store:{},
    card:{},
    userLocation: {},
    evaluate:{},  //评价信息
    isShopAttention:false,
    branchStoreCount:0,
    isOpen:false,
    isFollow: false,
    followNum:5,
    haveCoupons:false,
    indicatorDots: false,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin:0,
    nextMargin: 0,
    // 造型师
    designer:[],
    imgheights: [],
    //图片宽度 
    imgwidth: 750,
    //默认  
    current: 1,
    storeBanner: [],
    storeBannerDef:[
      { FilePath: "" }
    ],
    popHidden:true, //弹框
    coupons:[
      {
        name:'现金券',
        desc:'￥10元',
      },
      {
        name:'折扣券',
        desc:'7折',
      },
      {
        name:'体验券',
        desc:'',
      }
    ],
    couponsList:[
      {
        denomination:50,
        name:'新人无门槛优惠券',
        useRange:'限本店使用',
        dueDate:'2019-05-04',
        service:'剪发、理发',
        coin:500
      },
      {
        denomination:50,
        name:'新人无门槛优惠券',
        useRange:'限本店使用',
        dueDate:'2019-05-04',
        service:'剪发、理发',
        coin:500
      },
      {
        denomination:50,
        name:'新人无门槛优惠券',
        useRange:'限本店使用',
        dueDate:'2019-05-04',
        service:'剪发、理发',
        coin:500
      },
      {
        denomination:50,
        name:'新人无门槛优惠券',
        useRange:'限本店使用',
        dueDate:'2019-05-04',
        service:'剪发、理发',
        coin:500
      }
    ],
    services:[
      {
        name:'韩国的烫卷短',
        desc: '波浪小卷烫这款短发不仅显头型丰富饱满更是超显美范儿',
        imgSrc:'../../img/service/1.jpg',
        score:'9.8'
      },
      {
        name:'韩国的烫卷短',
        desc: '波浪小卷烫这款短发不仅显头型丰富饱满更是超显美范儿',
        imgSrc:'../../img/service/2.jpg',
        score:'9.8'
      },
      {
        name: '韩国的烫卷短',
        desc: '波浪小卷烫这款短发不仅显头型丰富饱满更是超显美范儿',
        imgSrc: '../../img/service/1.jpg',
        score: '9.8'
      },
      {
        name: '韩国的烫卷短',
        desc: '波浪小卷烫这款短发不仅显头型丰富饱满更是超显美范儿',
        imgSrc: '../../img/service/2.jpg',
        score: '9.8'
      }
    ],
    groupons:[
      {
        imgSrc:'../../img/groupon/1.jpg',
        name:'单人自然烫／创意染发二选一',
        dueDate:'2019年6月29日',
        originalPrice:'1999',
        grouponPrice:'29.9',
        grouponDesc:'三人闺蜜团',
        sales:'100'
      },
      {
        imgSrc:'../../img/groupon/2.jpg',
        name:'单人技术总监剪发',
        dueDate:'2019年6月29日',
        originalPrice:'1999',
        grouponPrice:'199',
        grouponDesc:'二人兄弟团',
        sales:'150'
      }
    ]
  },
  // 获取用户当前位置
  getLatLng(sid) {
    let that = this;
    utils.getUserLocation().then(res => {
      that.setData({
        userLocation: res
      }, function () {
        // that.GetStoreInfo(sid);
      })
    })
  },
  // 去店铺相册
  toStoreAlbum(){
    wx.navigateTo({
      url: `/p_storeAlbum/pages/storeAlbum/index?storid=${pageId}`,
    })
  },
  // 拨打电话
  callTel(e) {
    app.CallTel(e.currentTarget.dataset.tel)
  },
  // 打开地图导航
  getMap() {
    let _store = this.data.store;
    console.log(_store)
    utils.GetMapLocation(_store.StoreName, _store.EntpAddress, _store.GPS)
  },
  // 门店主页分享
  onShareAppMessage(res) {
    let sharePara = {
      title: pageTitle,
      path: `p_store/pages/B-homePage/index?StoreId=${pageId}`
    }
    wxReq.HttpRequst(false, 'api/Customer/CustomerShare',
      { customerId: _customerId, shareType: 1, dataId: pageId,  },
      'POST',
      function (result) {
        
      })
    return sharePara;
  },
  // 回到首页
  backToHome(){
    wx.switchTab({
      url: '/pages/showPlace/index'
    })
  },
  getChange(e){
    this.setData({
      current: e.detail.current+1
    })
  },
  // 激活会员
  activeCard(){
    let _store = this.data.store;
    let _card = this.data.card;
    wx.navigateTo({
      url: `/p_userCard/pages/activateCard/index?EntpId=${_store.EntpId}&StoreId=${pageId}`
    })
  },
  // 查看会员卡充值
  payMentCard(){
    let _card = this.data.card
    wx.navigateTo({
      url: `/p_card/pages/cardInfo/index?cardId=${_card.CardId}`,
    })
  },
  // 充值
  recharge(){
    wx.navigateTo({
      url: `/p_card/pages/cardPayment/index?BaseMoney=${this.data.store.AccountMoney}&storeId=${this.data.store.StoreId}&tabindex=0`,
    })
  },
  // 预约服务
  orderService(){
    let _store = this.data.store;
    wx.navigateTo({
      url: `/p_orderSuccess/pages/order/order?EntpId=${_store.EntpId}&StoreId=${_store.StoreId}&StoreName=${_store.StoreName}`,
    })
  },
  // 获取门店信息
  GetStoreInfo(storeId){
    isLoad = true;
    let that = this;
    wxReq.HttpRequst(false, 'api/MMC/GetStoreDetailById', { storeId: storeId, customerId: _customerId }, 'POST', (result) => {
      let _s = result.detailStore;

      wx.setNavigationBarTitle({
        title: _s.StoreName, //页面标题为路由参数
      })
      
      pageTitle = _s.StoreName;

      that.setData({
        store: _s,
        evaluate: result.OrderEvaluate,
        isShopAttention: result.isShopAttention,
        card: result.card,
        designer: result.storeUser
      });
    })
  },
  //关闭弹框
  closePop(){
    this.setData({
      popHidden:true
    })
  },
  //打开弹框
  openPop(){
    this.setData({
      popHidden:false
    })
  },
  // 获取优惠卷列表
  getCouponList(sid){
    let that = this;
    wxReq.HttpRequst(false, 'api/coupon/GetStoreCoupon',
      { storeId: sid, customerId: _customerId,top:3},
      'POST',
      function (result){
        console.log(result)
        that.setData({
          coupons: result
        })
      })
  },
  GetConvert(id){
    let that = this;
    if (JSON.stringify(this.data.card) == '{}' || !this.data.card){
      wx.showModal({
        title: '重要提示',
        content: '亲，您还不是该门店得会员，注册成为门店会员，每月都有优惠券送哦！',
        cancelText:'暂不注册',
        cancelColor:'#ddd',
        confirmText:'立即注册',
        confirmColor:'red',
        success: function (res) {
          if (res.confirm) {
            that.activeCard()
          }
        }
      })
    }else{
      wxReq.HttpRequst(false, 'api/coupon/GetFirstCoupon',
        { storeId: pageId, customerId: _customerId, ids: [id.currentTarget.dataset.id],isfree:0 },
        'POST',
        function (result) {

					if(result == ""){
            wx.showToast({
              title: '兑换成功',
              icon: "success"
            })
            that.getCouponList(pageId)
					}
        })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    let that = this;
    _customerId = wx.getStorageSync('sessionId')
    pageTitle = options.StoreName;
    pageId = options.StoreId;

    wx.setNavigationBarTitle({
      title: pageTitle, //页面标题为路由参数
    })
    // 获取门店相册
    this.GetStorePics(options.StoreId);
    if (options.userKm){
      _km = parseFloat(options.userKm);
      this.GetStoreInfo(options.StoreId);
    }else{
      // 获取用户当前位置
      this.getLatLng(options.StoreId)
    }
  },  

  // 关注店铺
  SetFollow(){
    let _store = !this.data.isShopAttention;
    let _text = false;
    let _num = this.data.store.ShopAttention ? this.data.store.ShopAttention:0;
    if(_store){
      _text = true
      _num++;
      app.SetAttention(this.data.store.StoreId,1,0);
    }else{
      _text = false;
      _num--;
      app.SetAttention(this.data.store.StoreId, 1,1);
    }
    this.setData({
      isShopAttention: _text,
      "store.ShopAttention":_num
    })
  },
  // 获取门店相册
  GetStorePics(storeId) {
    let that = this;
    wxReq.HttpRequst(false, 'api/MMC/GetStoreBannerById', 
        { storeId: storeId, customerId:_customerId },
         'POST',
         function(result){
           if (result.bannerList.length>0){
             that.setData({
               storeBanner: result.bannerList,
               current:1
             })
           }else{
             that.setData({
               current:0
             })
           }
    })
  },
  onShow:function(){
    _customerId = wx.getStorageSync('sessionId')
    if (_customerId != ""){
      this.GetStoreInfo(pageId);
      this.getLatLng(pageId)
      this.getCouponList(pageId);
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    let that = this;

    pageId = options.StoreId;

    _customerId = wx.getStorageSync('sessionId')

    // 获取门店相册
    this.GetStorePics(pageId);

  }
})