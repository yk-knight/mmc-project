Page({
  data: {

    active:false,
    orderData:{},
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 50,
    nextMargin: 50
  },
  // cancleOrder:function(){
  //   wx.redirectTo({
  //     url: '/pages/cancelOrder/index',
  //   })
  // },
  getMyOrder:function(){
    wx.redirectTo({
      url: '/p_center/pages/myOrder/index',
    })
  },
  onLoad:function(option){
    console.log(option);
    this.setData({
      orderData:option
    })
  }
})
