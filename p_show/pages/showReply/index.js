// pages/showReply/index.js
const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
let numIndex = 0;
var _commentId = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    replyData:{},
    sendText:"",
    isDisabled: true,
    replyName:''
  },
  // 取消回复
  cancleSend: function () {
    if (this.data.isDisabled) {
      this.setData({
        sendReady: false
      })
    }
  },
  // 键盘输入事件
  creatInput: function (e) {
    let inputVal = e.detail.value;
    if (inputVal != '') {
      this.setData({
        isDisabled: false
      })
    } else {
      this.setData({
        isDisabled: true
      })
    }
    this.setData({
      sendText: e.detail.value
    })
  },
  // 回复评论
  sendDiscuss(e){
    let that = this;
    utils.saveFormId(e.detail.formId);
    let _customerId = wx.getStorageSync("sessionId")
    let comId = '';
    if (numIndex >= 0 ){
      comId = this.data.replyData.BackData[numIndex].CommentId
    }else{
      comId = this.data.replyData.id
    }
    let commentData = {
      photoId: this.data.replyData.PhotosId,
      content: this.data.sendText,
      customerId: _customerId,
      commentId: comId
    }
    wxReq.HttpRequst(false, 'api/customerphoto/ReceivePhoto',
      commentData, "POST",
      function (res) {
          wx.navigateBack({
            url: `/p_show/pages/showInfo/index?photosId=${that.data.replyData.PhotosId}`,
          })
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // let _rd = options.rData;    
    // console.log(_rd)
    let _replyData = wx.getStorageSync('replayData')
    // let _replyData = JSON.parse(_rd)
    
    numIndex = parseInt(options.index);

    if (options.index >=0){
      
      let _backData = _replyData.BackData[numIndex];
      this.setData({
        replyData: _replyData,
        replyName: _backData.CommentNickName
      })
    }else{
      this.setData({
        replyData: _replyData
      })
    }
    

  },
  onUnload:function(){
    wx.removeStorageSync('replayData')
  }

})