const wxReq = require("../../../service/request.js")
let oid = ""; //orderId
let sid = "";
let bm = 0;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderPayInfo:[],
    payTypeId:"0",
    noChoose:"#eee",
    Choose: "#09bb07",
    totalPrice:0
  },
  // 选择支付方式
  checkReason: function (e) {
    let _option = e.currentTarget.dataset;

    this.setData({
      payTypeId: _option.index,
    })
  },
  // 确认支付
  paySubmit(){
    let that = this;
    wxReq.HttpRequst(false, "api/order/OrderPaySubmit",
      { submitData: that.data.orderPayInfo },
      'POST',
      function (res){
        wx.showToast({
          title: '支付成功',
        });
        setTimeout(function(){
          wx.navigateBack()
        },2500)
      })
  },
  // 获取支付信息
  getOrderPayInfo(){
    let that = this;
    wxReq.HttpRequst(false, 'api/order/OrderPayInfo',
      { orderId: oid },
      'POST',
      function (res) {
        let _totalPrice = 0;
        if(res){
          res.map(function (item) {
            _totalPrice = _totalPrice + item.ProPrice;
          })

          that.setData({
            orderPayInfo: res,
            totalPrice: _totalPrice
          })
        }else{
          wx.showModal({
            title: '充值提醒',
            content: '亲，您目前的会员账户无法支付本次消费',
            cancelText:'返回',
            confirmText:'去充值',
            success:(res) => {
              if(res.confirm){
                //去充值
                wx.redirectTo({
                  url: `/p_card/pages/cardPayment/index?BaseMoney=${bm}&storeId=${sid}&tabindex=0`,
                })
              };
              if(res.cancel){
                wx.navigateBack()
              }
            }
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    oid = options.orderId;
    sid = options.StoreId;
    bm = options.BaseMoney;
    this.getOrderPayInfo();
  },

})