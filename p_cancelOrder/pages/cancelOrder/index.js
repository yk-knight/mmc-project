// pages/cancleOrder/index.js
const wxReq = require("../../../service/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reason:[
      '预约错了，重新提交',
      '临时有事，下次再约',
      '人太多，不愿意等了',
      '想换一家店试试'
    ],
    cancelData:{
      orderId:'',
      cancelReson:'',
      cancelType:0
    },
    reasonId:0,
    customerId: wx.getStorageSync('sessionId')
  },
  checkReason: function(e){
    let _option = e.currentTarget.dataset;
    this.setData({
      reasonId: _option.id,
      'cancelData.cancelType': _option.id
    })
  },
  writeReason:function(e){
    this.setData({
      'cancelData.cancelReson': e.detail.value
    })
  },
  submitCancel:function(){
    let that = this;
    wxReq.HttpRequst(false, 'api/Customer/CancelAppointment',
      that.data.cancelData,
        'POST',
        function (result) {
          wx.showToast({
            title: '提交成功',
            icon: 'success',
            duration: 2000,
            success:function(e){
              setTimeout(function(){
                wx.navigateBack({
                  delta: 1
                },3000);
              })
            }
          })
        }

    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "cancelData.orderId": options.orderId
    })
  }
})