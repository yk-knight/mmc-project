const qqMapWx = require('qqmap-wx-jssdk.min.js')
const wxReq = require("../service/request.js")
const KEY = 'UE5BZ-6QVK6-OUOSO-EZ4MM-XET3Z-DIB6V';
const qqmapsdk = new qqMapWx({
  key: KEY
});
const config = require('../service/config.js')
let app = getApp();

const formatDate = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  // return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
  return [year, month, day].map(formatNumber).join('-') 
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

//todate默认参数是当前日期，可以传入对应时间 todate格式为2018-10-05
function getDates(days, todate) {
  var dateArry = [];
  for (var i = 0; i < days; i++) {
    var dateObj = dateLater(todate, i);
    dateArry.push(dateObj)
  }
  return dateArry;
}
function dateLater(dates, later) {
  let dateObj = {};
  let show_day = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
  let date = new Date(dates);
  date.setDate(date.getDate() + later);
  let day = date.getDay();
  let yearDate = date.getFullYear();
  let month = ((date.getMonth() + 1) < 10 ? ("0" + (date.getMonth() + 1)) : date.getMonth() + 1);
  let dayFormate = (date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate());
  dateObj.time = yearDate+"-"+month + '-' + dayFormate;
  dateObj.week = show_day[day];
  return dateObj;
}

// 根据生日的月份和日期，计算星座。
function getAstro(month, day) {
  var s = "魔羯水瓶双鱼牡羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯";
  var arr = [20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22];
  return s.substr(month * 2 - (day < arr[month - 1] ? 2 : 0), 2);
}
function initPage(){
  return {
    PageIndex: 0,
    PageSize: 10
  }
}
// 保存formid
function saveFormId(fid){
  let cid = wx.getStorageSync('sessionId');
  if (fid == 'the formId is a mock one'){
    return;
  }
  wxReq.HttpRequst(false, 'api/Customer/AddForm',
    { customerId: cid, formId: fid },
    'POST',
    function (res) {

    })
}
// 用户登录
function wxLogin(){

  return new Promise((resolve,reject) => {
    wx.login({
      success:(res)=>{

        resolve(res)
      },
      fail: (e) => {

        reject(e)
      }
    })
  })
}
//调用后台登录接口获取sessionId
function apiLogin(code){
  return new Promise((resolve,reject) => {
      wx.request({
        url: config.getBaseUrl() + 'api/WxOpen/OnLogin',
        dataType: "json",
        header: { 'content-type': 'application/json' },
        data: { code: code },
        method: "POST",
        success: (res) => {
          resolve(res);
        },
        fail: (e) => {
          reject(e)
        }
      });
    
  })
}
//获取用户当前坐标
function getUserLocation(){
  return new Promise((resolve,resject) => {
    wx.getLocation({
      success: (res) => {
        resolve(res);
      },
      fail: (e) => {
        resject(e)
      }
    });
  })
}
// 调用微信地图定位
function GetMapLocation(name,address,gps){
  wx.openLocation({
    name: name,
    address: address,
    latitude: gps.latitude,
    longitude: gps.longitude
  })
}

function GetUserInfo(){
  let userData = {
    type: 'userinfo',
    encryptedData: "",
    iv: "",
    sessionId: ""
  }

  userData.sessionId = wx.getStorageSync('sessionId');
  // 2、调用userInfo获取用户信息
  return new Promise((resolve, reject) => {
    wx.getUserInfo({
      success: function (e) {

        userData.encryptedData = e.encryptedData;
        userData.iv = e.iv;

        // 发现解码信息
        wxReq.HttpRequst(false, 'api/WxOpen/DecodeEncryptedData', userData, 'POST', (result) => {
        })
        resolve(e);

        // getUserLocation().then(res => {

        //   userData.latitude = res.latitude;
        //   userData.longitude = res.longitude;

          
        // }).catch(e => {
        //   console.log(e)
        //   resject(e)
        // })

      },
      fail: function () {
        //获取用户信息失败后。请跳转授权页面
        wx.showModal({
          title: '重要提示',
          content: '您还尚未登录，为了您能获得更好的体验，请先通过微信登录！',
          cancelText: "暂不登录",
          confirmText: "快速登录",
          cancelColor: "#ccc",
          confirmColor: "#179B16",
          success: function (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/p_getUserInfo/pages/getUserInfo/index',
              })
            }else{
              resolve(res);
            }
          }
        })
      }
    })
  })
 
}

//绘制图片
function drawImage() {
  //绘制canvas图片
  var that = this
  const ctx = wx.createCanvasContext('myCanvas')
  var bgPath = '../../../images/share_bg.png'
  var portraitPath = that.data.portrait_temp
  var hostNickname = app.globalData.userInfo.nickName

  var qrPath = that.data.qrcode_temp
  var windowWidth = that.data.windowWidth
  that.setData({
    scale: 1.6
  })
  //绘制背景图片
  ctx.drawImage(bgPath, 0, 0, windowWidth, that.data.scale * windowWidth)

  //绘制头像
  ctx.save()
  ctx.beginPath()
  ctx.arc(windowWidth / 2, 0.32 * windowWidth, 0.15 * windowWidth, 0, 2 * Math.PI)
  ctx.clip()
  ctx.drawImage(portraitPath, 0.7 * windowWidth / 2, 0.17 * windowWidth, 0.3 * windowWidth, 0.3 * windowWidth)
  ctx.restore()
  //绘制第一段文本
  ctx.setFillStyle('#ffffff')
  ctx.setFontSize(0.037 * windowWidth)
  ctx.setTextAlign('center')
  ctx.fillText(hostNickname + ' 正在参加疯狂红包活动', windowWidth / 2, 0.52 * windowWidth)
  //绘制第二段文本
  ctx.setFillStyle('#ffffff')
  ctx.setFontSize(0.037 * windowWidth)
  ctx.setTextAlign('center')
  ctx.fillText('邀请你一起来领券抢红包啦~', windowWidth / 2, 0.57 * windowWidth)
  //绘制二维码
  ctx.drawImage(qrPath, 0.64 * windowWidth / 2, 0.75 * windowWidth, 0.36 * windowWidth, 0.36 * windowWidth)
  //绘制第三段文本
  ctx.setFillStyle('#ffffff')
  ctx.setFontSize(0.037 * windowWidth)
  ctx.setTextAlign('center')
  ctx.fillText('长按二维码领红包', windowWidth / 2, 1.36 * windowWidth)
  ctx.draw();
}

//下载图片
function Downfiles(){
  wx.downloadFile({
    url: app.globalData.userInfo.avatarUrl,
    success: function (res1) {

      //缓存头像图片
      that.setData({
        portrait_temp: res1.tempFilePath
      })
      //缓存canvas绘制小程序二维码
      wx.downloadFile({
        url: that.data.qrcode,
        success: function (res2) {
          console.log('二维码：' + res2.tempFilePath)
          //缓存二维码
          that.setData({
            qrcode_temp: res2.tempFilePath
          })
          console.log('开始绘制图片')
          drawImage();
          wx.hideLoading();
          setTimeout(function () {
            that.canvasToImage()
          }, 200)
        }
      })
    }
  })
}

module.exports = {
  wxLogin, apiLogin, getUserLocation,
  formatDate: formatDate,
  getDates: getDates,
  getAstro: getAstro,
	initPage: initPage,
  GetMapLocation,
  GetUserInfo,
  saveFormId,
  qqmapsdk
}
