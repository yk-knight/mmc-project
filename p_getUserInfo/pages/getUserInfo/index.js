const wxReq = require("../../../service/request.js")
const Util = require('../../../utils/util.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  bindGetUserInfo: function (res) {
    var that = this;
    //此处授权得到userInfo
    let _detail = res.detail;
    let userData = {
          type: 'userinfo',
          encryptedData: _detail.encryptedData,
          iv: _detail.iv,
          sessionId: wx.getStorageSync('sessionId')
        }
      
    // 发现解码信息
    wxReq.HttpRequst(false, 'api/WxOpen/DecodeEncryptedData', userData, 'POST', (result) => {
      //最后，记得返回刚才的页面
      wx.setStorageSync("isBackGetInfo", true)
      wx.navigateBack({
        id: 1
      })
    })
    
    // Util.getUserLocation().then(res => {

    //   userData.latitude = res.latitude;
    //   userData.longitude = res.longitude;

    //   // 发现解码信息
    //   wxReq.HttpRequst(false, 'api/WxOpen/DecodeEncryptedData', userData, 'POST', (result) => {
    //     //最后，记得返回刚才的页面
    //     wx.setStorageSync("isBackGetInfo", true)
    //     wx.navigateBack({
    //       id: 1
    //     })
    //   })

    // }).catch(e => {
    //   wx.setStorageSync("isBackGetInfo", true)
    //   wx.navigateBack({
    //     id: 1
    //   })
    //   console.log(e)
    // })
    

  }
})