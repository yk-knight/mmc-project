const wxReq = require("../../../service/request.js")
let _options = {}
let app = getApp()
let _customerId = ""
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderInfo:{},
    orderService:[],
    orderImport:{}
  },
  // 拨打电话
  callTel(e){
    app.CallTel(e.currentTarget.dataset.tel)
  },
  // 获取订单详情
  getOrderDetil(infos){
    let that = this;
    wxReq.HttpRequst(false,
      'api/order/OrderInfo',
      { "orderId": infos.orderId },
      'POST',
      function(res){
        that.setData({
          orderInfo: res.Order,
          orderService: res.Service
        })
      })
  },
  // 去评价
  setReview(e){
    let oid = e.currentTarget.dataset.orderid;
    let oImt = this.data.orderImport;
    let oInfo = this.data.orderInfo;
    wx.navigateTo({
      url: `/p_review/pages/review/index?orderId=${oid}&storename=${oImt.storeName}&buname=${oInfo.BUName}&buheader=${oInfo.UserPhoto}`,
    })
  },
  // 去支付
  toPayment() {
    let _storeId = this.data.orderInfo.ShopId;
    let _entpid = this.data.orderInfo.EntpId;
    let oid = this.data.orderImport.orderId;
      // 判断是否激活会员
    wxReq.HttpRequst(false, 'api/customercard/GetCustomerCard ', { storeId: _storeId, customerId: _customerId }, 'POST', (result) => {
      if (!result) {
        wx.showModal({
          title: '会员激活',
          content: '亲，您还不是该店铺的会员',
          cancelText: '返回',
          confirmText: '去激活',
          success: (res) => {
            if (res.confirm) {
              //去激活
              wx.navigateTo({
                url: `/p_userCard/pages/activateCard/index?EntpId=${_entpid}&StoreId=${_storeId}`
              })
            };
            if (res.cancel) {
              wx.navigateBack()
            }
          }
        })
      } else {
        let _bm = 0;
        if (result.BalanceAmount){
          _bm = result.BalanceAmount;
        }
        wx.navigateTo({
          url: `/p_payment/pages/payment/index?BaseMoney=${_bm}&orderId=${oid}&StoreId=${_storeId}`,
        })
      }
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _options = options
    _customerId = wx.getStorageSync('sessionId');
    this.setData({
      orderImport: options
    }) 
    this.getOrderDetil(options)   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    if(!options){
      this.getOrderDetil(_options)
    }
  }
})