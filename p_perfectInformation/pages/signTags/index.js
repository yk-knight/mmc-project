const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
const app = getApp()
let _tags = [];
let _saveTags = [];
let _showTags = [];
Page({

  /**
   * 页面的初始数据
   */

  data: {
    tagArr:[],
    tagCount:0,
    TagsList:[],
  },
  // 完成并保存已选标签
  saveTags(e){
    utils.saveFormId(e.detail.formId);
    let _strSign = _saveTags.join(',')
    let _customerid = wx.getStorageSync("sessionId");
    wxReq.HttpRequst(false, 'api/Customer/UpdateCustomerSign', 
      { customerId: _customerid, strSign: _strSign},
      'POST',
      function(res) {
        console.log(_strSign)
        app.globalData.labelContent = _strSign
        wx.navigateBack()
      })
    

  },
  getTags(){
    let that = this;

    wxReq.HttpRequst(false, 'api/Customer/GetSAASSignByCustomer',{},
      'POST',
      function (res) {
        let _tList = res;

        // 标记已选择标签
        if (_saveTags.length > 0){
          _tList.map((value,index,array) => {
              let sa = value.Sign

              // 遍历默认标签数组
              sa.forEach((sVal,sInx,sArr) => {
                if (_saveTags.includes(sVal.name)) {
                    sa[sInx].check = true
                  }
              })
            
          })
        }
        
        that.setData({
          TagsList: _tList,
          tagCount: _saveTags.length
        });

      })
  },
  // 选择标签
  checkTags(e){

    let _eveData = e.currentTarget.dataset;
    let _value = _eveData.value;
    let _st = _saveTags.includes(_value)
    let tagsArray = `TagsList[${_eveData.in}].Sign[${_eveData.in2}].check`

    // 判断已选择的标签数组中是否包含当前选择的标签
    if(!_st){

      // 限制最多选择15个
      if(this.data.tagCount < 15){
        this.setData({
          [tagsArray]: true
        })
        _saveTags.push(_value); //不包含添加
      }else{
        return;
      }
    }else{
      this.setData({
        [tagsArray]: false
      })
      _saveTags.splice(_saveTags.indexOf(_value), 1) //包含则去除
    }

    // 更新已选择的标签数
    this.setData({
      tagCount: _saveTags.length
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _tagContent = app.globalData.labelContent
    let that = this;
    if (_tagContent){
      _saveTags = _tagContent.split(",")
    }
    this.getTags();
  }
})