// pages/myHistory/index.js
const app = getApp();
const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
let _type = 4
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noData:false,
    tabs: [{
        id:4,
        name:"全部"
      },{
        id:1,
        name:"待支付"
      },{
        id:2,
        name:"待评价"
      },{
        id:3,
        name:"未秀照"
      }
    ],
    tabIndex:4,
    history:[]
  },
  // 切换tab切
  tabChange(e) {
    let _index = e.currentTarget.dataset.index;
    // let isEmpty = this.SearchOrderList(_index);
    _type = _index;
    this.setData({
      tabIndex: _index
    })
    this.getMyOrder()
    
  },
  toShowPic:function(e){
    let _id = e.currentTarget.dataset.id;

    wx.navigateTo({
      url: `/p_showCreat/pages/showCreate/index?orderId=${_id}`,
    })
  },
  // 转去订单详情
  getDetails: function(e){
    let _index = e.currentTarget.dataset.index;
    let _htObj = this.data.history[_index]
    wx.navigateTo({
      url: `/p_history/pages/historyDetails/index?orderId=${_htObj.OrderId}&storeName=${_htObj.StoreName}&buName=${_htObj.BUName}&storePhoto=${_htObj.StorePhoto}`,
    })
  },
  // 我的订单记录
  getMyOrder() {
    let that = this;
    let _customerId = wx.getStorageSync("sessionId")
    wxReq.HttpRequst(false,
      'api/Customer/GetMyOrder',
      { customerId: _customerId, type: _type, },
      'POST',
      (res) => {

        if (res.OrderList.length < 1) {
          that.setData({
            noData: true
          })
        } else {
          that.setData({
            history: res.OrderList,
            noData: false
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options){
      _type = options.tabid
      this.setData({
        tabIndex: _type
      })
    }
    
    this.getMyOrder();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})