const utils = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
let _cosId = ''
Page({

  /**
   * 页面的初始数据
   */
  data: {
    myCoupons:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _cosId = wx.getStorageSync("sessionId")
    this.getMyCouponList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  // 获取我的优惠券列表
  getMyCouponList(){
    let that = this;
    wxReq.HttpRequst(false, "api/coupon/GetMyCoupon",
      { customerId: _cosId }, 'POST',
      function (res) {
        console.log(res)
        that.setData({
          myCoupons:res
        })
      })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  }
})