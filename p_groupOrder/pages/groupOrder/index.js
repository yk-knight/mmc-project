Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList:[
      {
        storeName: 'Ki.Mi创意发艺设计沙龙',
        orderTitle: '沙宣单人技术总监洗剪吹',
        coverImg: '../../img/cover-img.jpg',
        groupPrice: '199',
        originPrice: '599',
        requireNum: 1,
        memberList: [
          {
            memberImg: '../../img/userImg1.png'
          },
          {
            memberImg: '../../img/userImg2.png'
          },
          {
            memberImg: '../../img/userImg3.png'
          },
          {
            memberImg: '../../img/userImg4.png'
          },
          {}
        ]
      },
      {
        orderTitle: '沙宣单人技术总监洗剪吹',
        coverImg: '../../img/cover-img.jpg',
        groupPrice: '199',
        originPrice: '599',
        requireNum: 0,
        memberList: [
          {
            memberImg: '../../img/userImg1.png'
          },
          {
            memberImg: '../../img/userImg2.png'
          },
          {
            memberImg: '../../img/userImg3.png'
          },
          {
            memberImg: '../../img/userImg4.png'
          },
          {
            memberImg: '../../img/userImg5.png'
          }
        ]
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})