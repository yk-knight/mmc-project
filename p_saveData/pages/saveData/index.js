const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  saveData(e){
    utils.saveFormId(e.detail.formId)
    let _cid = wx.getStorageSync("sessionId")
    let _saveData = e.detail.value;
    _saveData.CustomerId = _cid
    wxReq.HttpRequst(false, "/api/newstore/Create",
      _saveData, 'POST',
      function (res) {
        wx.showModal({
          title: '提交成功',
          showCancel:false,
          confirmText:"返回首页",
          content: '亲，您的资料已经提交成功，虫虫很快会跟您联系哒！',
          success(res) {
            if (res.confirm) {
              wx.switchTab({
                url: '/pages/index/index',
              })
            }
          }
        })
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  }
})