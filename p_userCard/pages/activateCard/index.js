const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
var _storeid = 0;
var _entpid = 0;
let telphoto = 0;
let _cid = "";
let ct;
let waitCodeTime = 60;
let _cardid = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    telphoto:"",
    codeUse:true,
    codeTime: waitCodeTime,
    couponList:[]
  },
  // 激活会员卡
  cardFormSubmit(event){

    utils.saveFormId(event.detail.formId);
    this.setData({
      couponList: []
    })
    let that = this;
    let _tel = event.detail.value.tel;
    let _code = ""+event.detail.value.code;
    if(_tel == "" || _code == ""){
      wx.showToast({
        title: "请将信息填写完整!",
        icon: 'none'
      })
    }else{
      
      wxReq.HttpRequst(false, 'api/customercard/InitCard',
        { customerId: _cid, telephone: _tel, code: _code, storeId: _storeid, entpId: _entpid },
        'POST',
        function (res) {
          let _myCard = res;
          if (res.msg != "") {
            wx.showToast({
              title: res.msg,
              duration: 1500,
              success: function () {
              }
            })
          } 
          else {

            wxReq.HttpRequst(false, 'api/coupon/GetStoreFirstCoupon',
              { storeId: _storeid },
              'POST',
              function (res) {
                if (res.length > 0) {

                  wx.showModal({
                    title: '恭喜激活成功！',
                    content: "本店免费赠送您两张优惠券，可以前往“我的优惠券”中查看",
                    cancelText: '返回',
                    confirmText: '立即查看',
                    success(res) {
                      if (res.confirm) {
                        wx.redirectTo({
                          url: `/p_coupons/pages/coupon/index`
                        })
                      } else {
                        wx.navigateBack({
                          delta: 1
                        })
                      }
                    }
                  })
                } else {
                  wx.showToast({
                    title: "激活成功!",
                    duration: 1500,
                    success: function () {
                      // 提交后台
                      setTimeout(function () {
                        wx.redirectTo({
                          url: `/p_card/pages/cardPayment/index?StoreId=${_storeid}`
                        })
                      }, 2000)
                    }
                  })
              }

            })  
          }
        }
      )
    }
  },
  // 计时器
  setCodeTime(){
    let that = this;
    if (that.data.codeTime > 0){
      this.setData({
        codeTime: that.data.codeTime - 1
      })
      ct = setTimeout(function () {
        that.setCodeTime()
      }, 1000); 
    }else{
      clearTimeout(ct);
      this.setData({
        codeUse:true,
        codeTime: waitCodeTime
      })
    }
    
  },
  // 点击获取验证码
  getCode(){
    if(telphoto != ""){
      this.setData({
        codeUse:false
      })
      this.setCodeTime()
      wxReq.HttpRequst(false, 'api/customercard/GetFlagCode',
        { customerId: _cid, telephone: telphoto, storeId: _storeid },
        'POST',
        function (res) {

        })
    }else{
      wx.showToast({
        title: '请先填写手机号',
        icon: 'none',
        duration: 2000
      })

    }
    
  },
  inputTel(e){
      telphoto = e.detail.value
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _cid = wx.getStorageSync('sessionId')
    _storeid = parseInt(options.StoreId) ;
    _entpid = parseInt(options.EntpId);
    _cardid = options.cardId;
  }
})