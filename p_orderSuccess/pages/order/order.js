const Util = require('../../../utils/util.js')
const wxReq = require("../../../service/request.js")
var app = getApp();
var requestData = {};
var submitData = {
      buId: 0,
      buName: '',
      description: "",
      entpJobId: "",
      orderServices:[],
      photoId: ""
    };
var _option = {}
let dataTime = []
let today = ""
Page({
  data: {
    action:'dateAction',
    timeAction:'timeaciton',
    serverAction:'action',
    num:0,
    switchCheck:true,
    timeNum:-1, 
    serverNum:-1,
    orderDate:[],
    times: [],
    designer:[], 
    designerId:-1,
    servers:[],
    date:'',
    time:'',
    fullYear:''
  },
  // 日期比较
  DateCompare:function(newDay){
    let _today = today.split("-");
    let _newDay = newDay.split("-");

    for(var n=0;n<3;n++){
      if (parseInt(_newDay[n]) > parseInt(_today[n])){
        return false
      }
    }
    return true
  },
  // 切换日期时间
  ChangeDateTime(e){
    let _index = e.currentTarget.id;
    let _data = e.currentTarget.dataset;
    let type = _data.type

    if(type == "date"){
      if (_index == this.data.num){
        return;
      }
      this.showTime(this.DateCompare(_data.date));
      dataTime.splice(1, 1);
      
      dataTime[0] = _data.date;
      this.setData({
        num: _index,
        timeNum: -1
      });

    }else{

      dataTime[1] = _data.time + ":00";
      
      this.setData({
        timeNum: _index,
      })
    }
    submitData.serviceDateTime = dataTime.join(" ");
    this.GetDsigner();
  },

  // 选择需要预约的服务
  _checkService: function (e) {
    let _index = e.currentTarget.id;
    let _service = e.currentTarget.dataset
    let _os = {
      proName: _service.proname,
      ServiceDic: _service.storeprojectid
    }
    submitData.orderServices[0] = _os
    this.setData({
      serverNum: _index
      // [OrderService]: _os
    })
    this.GetDsigner()
  },

  // 日期周期推算
  GrderDateAround:function() {
    let time = Util.formatDate(new Date())
    let date = Util.getDates(7,time);
    today = time
    // 初始化日期
    dataTime[0] = time;
    this.setData({
      orderDate:date,
    })
  },

  // 提交预约订单
  getOrder(e){
    let that = this;
    Util.saveFormId(e.detail.formId);
    submitData.serviceDateTime = dataTime.join(" ");

    if (dataTime.length > 1 && submitData.orderServices.length > 0 && submitData.buId >= 0){
        wxReq.HttpRequst(false, 'api/Customer/AddAppointData', submitData, 'POST',
        (result) => {
     
          if(result.msg == ""){
            let orderService = submitData.orderServices.map(function (item) {
              return item.proName
            });
            wx.redirectTo({
              url: `/p_orderSuccess/pages/orderSuccess/index?storeName=${requestData.StoreName}&orderTime=${submitData.serviceDateTime}&proName=${orderService.join(",")}&buName=${submitData.buName}`,
            })
          }else{
            wx.showToast({
              title: result.msg,
              icon: 'none',
              duration: 2000,
              success: function (e) {

              }
            })
          }
          
      })
    }else{
      let connext = "亲，信息还没有填完哦！";
      wx.showToast({
        title: connext,
        icon: 'none',
        duration: 2000,
        success: function (e) {
          
        }
      })
    }
    
  },
  // 预约时间刷新
  showTime(isToday){
    let _starTime = 10,
        _endTime = 21;
    let timeArray = [];
    let that = this;
    let _orderDate = new Date().getHours();
    if (isToday){
      // 根据当前时间，展示可选的时间段
      for (let n = _starTime; n <= _endTime; n++) {
        let timeObj = {
          time: [n, '00'].join(":"),
          toOrder: n <= _orderDate ? true : false
        }
        timeArray.push(timeObj);
      }

      timeArray.some(function(item,index){
        if(!item.toOrder){
          dataTime[1] = item.time;
          that.setData({
            timeNum: index
          })
          return index;
        }else{
          // 当天已经不能预约了
        }
      })
      
    }else{
      // 根据当前时间，展示可选的时间段
      for (let n = _starTime; n <= _endTime; n++) {
        let timeObj = {
          time: [n, '00'].join(":"),
          toOrder: false
        }
        timeArray.push(timeObj);
      }
    }
    this.setData({
      times: timeArray
    }) 
  },
  // 是否选择技师
  ChangeTechnician(e){

    if(e.detail.value){
      this.setData({
        designerId: -1
      });
      submitData.buId = 0,
      submitData.buName = ""
    }else{
      submitData.buId = -1
    }
  },
  // 预约技师
  orderSinger(e){
    let _Singer = e.currentTarget.dataset
    this.setData({
      designerId:_Singer.index,
      switchCheck:false
    });
    submitData.buId = _Singer.buid,
    submitData.buName = _Singer.buname
  },
  // 技师主页直接预约进入
  orderDesigner(uid){
    let that = this;
    this.data.designer.map((val,index) => {

      if(val.UID == uid){
        that.setData({
          designerId: index,
          switchCheck: false
        });
        submitData.buId = uid,
        submitData.buName = val.RealName
      }
    })
  },
  // 点击去技师主页
  toDesignerHome(e){
    let _data = e.currentTarget.dataset;
    wx.navigateTo({
      url: `/p_designHome/pages/designerHome/index?uid=${_data.uid}&storeId=${_data.storeid}&realName=${_data.buname}`,
    })
  },

  // 进入页面调用预约
  GetAppointInit:function(opt){
    let that = this;
    let _dateTime = dataTime.join(" ")
    let _customerId = wx.getStorageSync('sessionId');
    requestData = {
      EntpId: opt.EntpId,
      StoreName:opt.StoreName,
      StoreId: opt.StoreId,
      customerId: _customerId,
      dateTime: _dateTime
    }
    submitData.entpId = requestData.EntpId,
    submitData.storeId = requestData.StoreId,
    submitData.customerId = requestData.customerId,
    submitData.serviceDateTime = requestData.dateTime,

    wxReq.HttpRequst(false, 'api/Customer/GetAppointInitData', requestData, 'POST',
      (result) => {

        that.setData({
        servers: result.serviceList,
        designer: result.userList
      },function(){
        if (opt.uid && opt.uid != "") {
          that.orderDesigner(opt.uid)
        }
      });
    })
  },

  // 获取技师列表
  GetDsigner:function(){
    let reqData = {
      customerId: wx.getStorageSync('sessionId'),
      storeId: parseInt(_option.StoreId),
      entpId: _option.EntpId,
      dateTime: submitData.serviceDateTime,
      StoreProjectId: submitData.orderServices.length > 0 ? submitData.orderServices[0].storeProjectId : 0,
      serviceDic: submitData.orderServices.length > 0 ?submitData.orderServices[0].ServiceDic:""
    }
    wxReq.HttpRequst(false, 'api/Customer/CheckAppointData', reqData, 'POST',
      (result) => {
        this.setData({
          designer: result.userList
        });
      })
  },
  onShow:function(){
    submitData.buId = 0
    submitData.buName = ""
    submitData.orderServices = []
  },
  // 初始化页面数据
  onLoad:function(option){
    _option = option;
    console.log(_option)
    submitData.photoId = _option.photoId;
    this.showTime(true);
    this.GrderDateAround();
    this.GetAppointInit(option)
  },
  // 页面卸载
  onUnload: function(){
    submitData.orderServices = [];
  }
})