// pages/message/index.js
const app = getApp();
const wxReq = require("../../../service/request.js")
let cosId=""
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: ['评论我的', '回复我的'],
    tabIndex:0,
    newmsg:0,
    replyData:[],
    commentData:[],
  },
  tabChange(e){
    let _index = e.currentTarget.dataset.index
    if(_index != this.data.tabIndex){
      wx.pageScrollTo({
        scrollTop: 0,
        duration: 200
      })
      this.setData({
        tabIndex: _index
      })
      if (_index == 1) {
        this.UpdateNewTime();
      } else {
        this.UpdateNewTimeCommand()
      }
    }
    
  },
  // 去到对应的秀贴
  toPhotos(event){
    let photoInfo = event.currentTarget.dataset;
    wx.navigateTo({
      url: `/p_show/pages/showInfo/index?customerId=${photoInfo.customerid}&photosId=${photoInfo.photosid}`
    })
  },
  // 回复我的
  GetReplyMyComment(_customerId){
    let that = this;
    let cId = '96B3715F-78B2-4527-90FC-04BA19D69124';
    wxReq.HttpRequst(false, 'api/customerphoto/GetReplyForMyComment',
      { customerId: _customerId},
        'POST',
        function(res){
          that.setData({
            replyData:res
          })
        }
    )
  },

  // 评论我的
  GetComment(_customerId) {
    let that = this;
    wxReq.HttpRequst(false, 'api/customerphoto/GetCommentByMyPhoto',
      { customerId: _customerId },
      'POST',
      function (res) {
        that.setData({
          commentData: res
        })
      }
    )
  },
  UpdateNewTime(){
    let that = this;
    wxReq.HttpRequst(false, 'api/Customer/UpdateNewMsgTime',
      { customerId: cosId },
      'POST',
      function (res) {
        that.setData({
          newmsg: 0
        })
      })
  },
  // 更新用户的最新消息的时间[评论我的]
  UpdateNewTimeCommand() {
    wxReq.HttpRequst(false, 'api/Customer/UpdateNewMsgTime_Command',
      { customerId: cosId },
      'POST',
      function (res) {
      })
  },
  GetNewMsg(){
    let that = this
    wxReq.HttpRequst(false, 'api/Customer/MyMsgTip',
      { customerId: cosId },
      'POST',
      function (res) {
        that.setData({
          newmsg: res.newmsg
        })
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    cosId = wx.getStorageSync('sessionId')
    this.GetNewMsg();
    this.UpdateNewTimeCommand();
    this.GetReplyMyComment(cosId);
    this.GetComment(cosId)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

})