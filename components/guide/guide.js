// components/guide/guide.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    guideData: {
      type: Array,
      value: []
    },
    isHide: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    indicatorDots: false,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2500,
    duration: 500,
    previousMargin: 0,
    nextMargin: 0,
    current: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getChange(e) {
      this.setData({
        current: e.detail.current
      })
    },
    closeShade(){
      this.setData({
        isHide: true
      })
      wx.setStorageSync('isPopHide', true);
    }
  }
})
