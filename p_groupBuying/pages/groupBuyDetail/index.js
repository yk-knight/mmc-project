const app = getApp();
const wxReq = require("../../../service/request.js")
let _customerId = "";
let endTime = "";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    groupContent: {
      title: "沙宣单人技术总监洗剪吹",
      groupPrice: "199",
      originalPrice: "599",
      groupedNum: "20",
      coverSrc: "../../img/cover-img.jpg",
    },
    groupMember: [
      {
        IsLeader: true,
        memberImg: '../../img/userImg1.png'
      },
      {},
      {}
    ],
    requestNum: 2,
    timeObj: {},
    recommend: [
      {
        coverSrc: '../../img/recommend-cover1.jpg',
        title: '【技术总监组】洗剪吹',
        price: '66'
      },
      {
        coverSrc: '../../img/recommend-cover1.jpg',
        title: '【技术总监组】洗剪吹',
        price: '66'
      }
    ]
  },
  // 支付接口测试
  orderService(){
    wxReq.HttpRequst(false,
      'api/WxOpen/WXJsApiPay',
      { Fee:1,customerId: _customerId,OrderBusiness:2,Description:"测试支付"},
      'POST',
      (res) => {
        console.log(res);
        let _data = res;
        wx.requestPayment({
          timeStamp: _data.timeStamp,
          nonceStr: _data.nonceStr,
          package: _data.package,
          signType: _data.signType,
          paySign: _data.paySign,
          success: function (res) {
            wx.showModal({
              title: '支付成功',
              content: '',
            })
          },
          fail: function (res) {
            console.log(res);
          }
        })
      })
  },
  //小于10的格式化函数
  timeFormat(param) {
    return param < 10 ? '0' + param : param;
  },
  countDown(){
    let _newTime = new Date().getTime();
    let _endTime = new Date(endTime).getTime();
    let _timeObj = {};

    if (_endTime - _newTime > 0) {
      console.log(111)
      //将毫秒数的time转为分秒
      let time = (_endTime - _newTime) / 1000;
      // 获取时、分、秒、分秒
      _timeObj = this.dealerTime(time)

    } else {
      //活动已结束，全部设置为'00'
      _timeObj.hour = '00';
      _timeObj.min = '00';
      _timeObj.sec = '00';
    }
    this.setData({ timeObj: _timeObj })
    setTimeout(this.countDown, 1000);
  },
  // 处理时间
  dealerTime(time) {
    // 保留到分秒，所以此处time为分秒数
    let _timeObj = new Object();
    _timeObj.hour = this.timeFormat(parseInt(time / 3600));
    _timeObj.min = this.timeFormat(parseInt(time % 3600 / 60));
    _timeObj.sec = this.timeFormat(parseInt(time % 3600 % 60));

    return _timeObj;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _customerId = wx.getStorageSync('sessionId');
    endTime = options.endTime;
    this.countDown()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage(res) {
    var pageTitle = this.data.groupContent.title;
    return {
      title: pageTitle,
      path: `p_groupBuying/pages/groupBuyDetail/index`
    }
  },
})