
Page({

  /**
   * 页面的初始数据
   */
  data: {
    codeMsg:{},
    price:"",
    tabIndex:"",
    cardName:""
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if (options){
      this.setData({
        codeMsg: JSON.parse(options.data),
        price: options.price,
        tabIndex:options.tabIndex,
        cardName:options.cardName
      })
    }
  },

})