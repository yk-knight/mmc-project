const wxReq = require("../../../service/request.js")
const utils = require('../../../utils/util.js')
const amapFile = require("../../../utils/amap-wx.js");
var myAmapFun = new amapFile.AMapWX({ key: "c898d42f6c8c8a3c23e470a1e111beb4" });
let pages = utils.initPage();
const app = getApp();
let _cusId =  "";
let apiData = {};
let _keyWord = "";
let _procode = "";
let sortType = 1;
let _cid = '';
let _lat = 0;
let _lng = 0;
let _storeList = "";
let _phone = '';
let _storeId = '';
let _storeName = '';
let _buName = '';
let _buid = '';
let storeInfo = {};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isEnd: false,
    isLoading: false,
    topLoading: false,
    searchWord: '',
    storeList:[],
    hasSearched: false,
    choosedIndex: -1,  //saas平台门店选择
    searchStore: [],   //地图搜索的门店
    isSearch: false,  //是否切换搜索门店
    markers: [{  //地图选择的位置标记
      id: 0,
      latitude: 0,
      longitude: 0,
      width: 50,
      height: 50
    }],
    isSelected: false, //是否从地图选择了门店
    selectedName: "", //非saas平台选择的门店
    selectedAddress: ""
  },


  //获取店铺列表
  GetStoreList: function (_page) {

    let that = this;
    apiData.customerId = _cusId;
    apiData.p = _page;
    apiData.sortby = sortType;
    apiData.keyword = this.data.searchWord
    apiData.procode = _procode
    apiData.ViewDic = this.data.viewDicIndex

    if (_page.PageIndex == 0) {
      this.setData({
        topLoading: true
      })
    }
    wxReq.HttpRequst(false, 'api/store/GetStoreList',
      apiData, 'POST',
      (result) => {
        console.log(result)
        this.setData({
          topLoading: false
        })
        // if (result.length>0) {
            that.setData({
              storeList: result,
              hasSearched: true
            })
          _storeList = result;
        // }
      })
  },

  //输入筛选门店
  watchInput(e){
    this.setData({
      searchWord: e.detail.value
    })
  },

  // 筛选门店
  goScreen(){
    pages.PageIndex = 0;
    this.GetStoreList(pages)
  },

  // 选择门店
  chooseStore(e){
    let that = this;
    let _thisIndex = e.currentTarget.dataset.index;
    that.setData({
      choosedIndex: _thisIndex
    })
    _storeId = _storeList[_thisIndex].StoreId;
    _storeName = _storeList[_thisIndex].StoreName;
  },

  // 绑定sass门店
  bindStore(){
    wxReq.HttpRequst(false, 'api/storeuser/BindBUStore',
      { customerId: _cusId, storeId: _storeId, tel: _phone, flag: 0}, 'POST',
      (result) => {
        console.log(result)
        if(result.code != "0"){
          if(result.code == "02"){
            //技师不在该门店
            wx.showModal({
              title: '',
              content: `对不起，在店铺${_storeName}中没有找到您的技师信息`,
              success(res) {
                if (res.confirm) {
                  // console.log('用户点击确定')
                } else if (res.cancel) {
                  // console.log('用户点击取消')
                }
              }
            })
          }
        }else{
          // 验证成功
          _buName = result.buname;
          _buid = result.buid;
          wx.showModal({
            title: '',
            content: `确认绑定到${_storeName}的手艺人【${_buName}】`,
            success(res) {
              if (res.confirm) {
                wxReq.HttpRequst(false, 'api/storeuser/BindBUStore',
                  { customerId: _cusId, storeId: _storeId, tel: _phone, flag: 1 }, 'POST',
                  (result) => {
                    console.log(result)
                    if(result.code == "0"){
                      wx.showToast({
                        title: '绑定成功！',
                        duration: 1500,
                        success: function () {

                          setTimeout(function () {
                            wx.navigateTo({
                              url: `/p_designHome/pages/designerHome/index?uid=${_buid}&storeId=${_storeId}`,
                            })
                          }, 1500)

                        }
                      })
                    }
                  })
              } else if (res.cancel) {
                // console.log('用户点击取消')
              }
            }
          })
        }
      })
  },

  // 通过地图搜索选择门店
  searchStore(){
    this.setData({
      isSearch: true
    })
    let that = this;

  },

  //输入搜索门店
  inputStore(e){
    let that = this;
    _keyWord = e.detail.value;
    that.setData({
      isSelected: false
    })
    myAmapFun.getInputtips({
      keywords: _keyWord,
      city: "南京",
      location: 'apiData.longitude,apiData.latitude',
      success: function (data) {
        //成功回调
        that.setData({
          searchStore: data.tips
        })
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    })
  },

  // 点击搜索
  goSearch(){
    let that = this;
    myAmapFun.getInputtips({
      keywords: _keyWord,
      city: "南京",
      location: 'apiData.longitude,apiData.latitude',
      success: function (data) {
        //成功回调
        that.setData({
          searchStore: data.tips,
          isSelected: false
        })
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    })
  },

  //取消搜索
  cancelSearch(){
    this.setData({
      isSearch: false,
      isSelected: false,
      searchStore: []
    })
  },

  //选择非saas门店
  selectStore(e){
    let _location = e.currentTarget.dataset.location;
    let _name = e.currentTarget.dataset.name;
    let _address = e.currentTarget.dataset.address;
    let [ _lng, _lat ] = _location.split(',');
    this.setData({
      selectedName: _name,
      selectedAddress: _address,
      isSelected: true,
      'markers[0].latitude': _lat,
      'markers[0].longitude': _lng
    })
    storeInfo = {
      storeName: _name,
      address: _address,
      longitude: _lng,
      latitude: _lat
    }
  },

  // 搜索门店确认
  confirmSearchStore(){
    wx.setStorageSync("choosedStoreInfo", JSON.stringify(storeInfo));
    wx.navigateBack({
      delta: 1
    })
    let paramObj = {...{ customerId: _cusId },...storeInfo};
    wx.showModal({
      title: '',
      content: `确认绑定到门店${storeInfo.storeName}`,
      success(res) {
        if (res.confirm) {
          wxReq.HttpRequst(false, 'api/storeuser/BindBUStore_GPS', paramObj, 'POST',
            function (result) {
              console.log(result)
              wx.showToast({
                title: '绑定成功！',
                duration: 1500,
                success: function () {

                  setTimeout(function () {
                    wx.navigateTo({
                      url: `/p_authenticate/pages/authenticatedHome/index`,
                    })
                  }, 1500)

                }
              })
            })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _phone = options.tel
    let that = this;
    _storeList = wx.getStorageSync("storeList");
    if (_storeList) {
      this.setData({
        storeList: _storeList
      })
    }
    apiData.viewcode = options.viewcode;
    _cusId = wx.getStorageSync("sessionId");

    // 提供后台当前坐标用于排序
    utils.getUserLocation()
      .then(res => {
        _lat = res.latitude;
        _lng = res.longitude;
        apiData.latitude = res.latitude;
        apiData.longitude = res.longitude;
        this.GetStoreList(pages)

        this.setData({
          'markers[0].latitude': res.latitude,
          'markers[0].longitude': res.longitude
        })

      }).catch(res => {

        console.log(res)

      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

    if (this.data.isEnd) {
      return;
    }
    this.setData({
      isLoading: true
    })
    pages.PageIndex++
    this.GetStoreList(pages);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})