// pages/chooseCity/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    thisCity:"",
    citys:[
      {
        name:'history',
        list:['南京','马鞍山']
      },{
        name: 'hot',
        list: ['南京', '上海','北京','呼和浩特']
      }
    ]
  },
  setCityChoose:function(e) {
    if (e.target.dataset.name != app.globalData.city){
      app.globalData.city = e.target.dataset.name;
      app.globalData.isCityChange = true;
      wx.setStorageSync('city', e.target.dataset.name);
    }
    
    wx.reLaunch({
      url: '/pages/index/index'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      thisCity: app.globalData.city
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})